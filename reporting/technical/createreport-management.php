<?php

require_once('base.inc.php');

class ryteReport extends ryteBase {

  public function __construct () {

/*
		MAI       KW 21
    JUNI      KW 23
    JULI      KW 27
    AUGUST    KW 32
    SEPTEMBER KW 36
    OKTOBER   KW 41
    NOVEMBER  KW 44
    DECEMBER  KW 49
    JANUARY   KW 02    
*/

    $year0 = date("Y", strtotime('-5 month'));
    $year1 = date("Y", strtotime('-4 month'));
    $year2 = date("Y", strtotime('-3 month'));
    $year3 = date("Y", strtotime('-2 month'));
    $year4 = date("Y", strtotime('-1 month'));    
  	$year5 = date("Y", strtotime('now')); 

    $month0 = date("F", strtotime('-5 month'));
    $month1 = date("F", strtotime('-4 month'));
    $month2 = date("F", strtotime('-3 month'));
    $month3 = date("F", strtotime('-2 month'));
    $month4 = date("F", strtotime('-1 month'));
    $month5 = date("F", strtotime('now'));

    $week0 = '41';
    $week1 = '45';
    $week2 = '49';
    $week3 = '01';
    $week4 = '06';    
    $week5 = '10';    

    $compare = array();
    $compare[$week0] = array('mo' => $month0, 'ye' => $year0);
    $compare[$week1] = array('mo' => $month1, 'ye' => $year1); 
    $compare[$week2] = array('mo' => $month2, 'ye' => $year2);
    $compare[$week3] = array('mo' => $month3, 'ye' => $year3);
    $compare[$week4] = array('mo' => $month4, 'ye' => $year4);
    $compare[$week5] = array('mo' => $month5, 'ye' => $year5);    

    $reports = array();

    foreach ($this->projects as $ryteId) {

      foreach ($compare as $week => $date) {
        $data = $this->readReports($date['ye'].$week, $ryteId);
        $reports[$ryteId][$week] = array($date['ye'], $date['mo'], $data);
      }

    }

    $this->seochecksReduced = array();
    foreach ($this->seochecks as $keyname => $arr) {
      $this->seochecksReduced = array_merge($this->seochecksReduced, $arr);
    }

    $this->createReport($reports);

  }


  private function createReport ($reports) {

    $reportdata_bmw = array();
    $reportdata_mini = array();    

    foreach ($reports as $market => $weeks) {

      foreach ($weeks as $week => $dataset) {

        $year  = $dataset[0];
        $month = date('m', strtotime($dataset[1]));
        $data  = $dataset[2];
        echo $date  = '01.' . $month .'.'. $year;
        echo PHP_EOL;

        foreach ($data as $name => $c) {
          
          $url    = $this->desc[$market][0];
          $name   = $this->seochecksReduced[$name][0];

          if (stripos($url,'mini') !== FALSE) {
            $reportdata_mini[] = array($url, $name, $c, $date);            
          } else {
            $reportdata_bmw[] = array($url, $name, $c, $date);            
          }

        }

      }

    }

    $this->writeCsv($reportdata_bmw, 'bmw');
    $this->writeCsv($reportdata_mini, 'mini');    


  }



  private function writeCsv ($fields, $name) {

    $fn = PATH.STOREMANAGEMENT.'/'.$name.'.csv';

    $fp = fopen($fn, 'w');
    foreach ($fields as $field) {
      fputcsv($fp, $field);
    }

    fclose($fp);
    
  }


  private function readReports ($week, $ryteId) {

    $result = array();

      foreach ($this->aggregatechecks as $checkname) {

        $fn = PATH.STORE.$week.'/'.$checkname.'_'.$ryteId.'.csv';

        $file = @fopen($fn, 'r');

        $arr = array();

        if ($file !== false) {

          while (($line = fgetcsv($file)) !== FALSE) {
            $arr[] = $line[0];
          }          
          fclose($file);

        } else {

          $arr = false;

        }

        $result[$checkname] = count($arr) - 2;

      }

    return $result;

  }


}

new ryteReport();