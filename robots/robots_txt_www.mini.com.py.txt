User-agent:*
Disallow:/content/
Allow:/content/dam/MINI/
Disallow:/?
Disallow:/standard-selector/.*.html
Disallow:/disclaimers/.*.html
Disallow:/why-mini-campaign/.*.html
Disallow:/mit-pages/.*.html
Disallow:/campaign-templates/.*.html
Disallow:/home-stageteaser/.*.html
Disallow:/model-selection/.*.html
Disallow:/mit-pages/.*.html
Sitemap: https://www.mini.com.py/es_PY/home.sitemap.xml
