<?php

// 02

define('DIR', dirname(__FILE__));

class checkRedirects
{

  private $market     = 'DE';

  public function __construct ()
  {

    $this->readCsv1();
    $this->readCsv2();

    array_shift($this->baseAKM1);
    array_shift($this->baseAKM2);

    $finalAkamai = array();

    $i = 1;

    foreach ($this->baseAKM1 as $key => $vala) {


      if (stripos($vala[0], 'DE') === FALSE) {
        continue;
      }


      if (substr( $vala[2], 0, 4 ) !== 'http') {
        continue;
      }

      $finalAkamai[$vala[1]] = array($this->market.$i, $vala[1], $vala[2], 301);
      $i++;

    }

    foreach ($this->baseAKM2 as $key => $valb) {

      if (stripos($valb[0], 'DE') === FALSE) {
        continue;
      }

      if (substr( $valb[2], 0, 4 ) !== 'http') {
        continue;
      }

      if (!isset($this->baseAKM1[$valb[1]])) {
        $finalAkamai[$valb[1]] = array($this->market.$i, $valb[1], $valb[2], 301);
        $i++;
      }

    }


    $this->writeResultAkamai(array('ruleName','matchURL','result.redirectURL','result.statusCode'));

    foreach ($finalAkamai as $key => $value) {
      $this->writeResultAkamai($value);         
    }

    echo count($finalAkamai);

  }

  private function readCsv1 () {
    $this->baseAKM1 = array ();   
    $file = fopen(DIR. '/result-akamai-1.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $this->baseAKM1[$line[1]] = array($line[0], $line[1], $line[2], $line[3]);
    }
    fclose($file);
  }

  private function readCsv2 () {
    $this->baseAKM2 = array ();   
    $file = fopen(DIR. '/result-akamai-2.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $this->baseAKM2[$line[1]] = array($line[0], $line[1], $line[2], $line[3]);
    }
    fclose($file);
  }


  private function writeResult ($fields) {
    $fp = fopen(DIR . '/result.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResultError ($fields) {
    $fp = fopen(DIR . '/result-error.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResultAkamai ($fields) {
    $fp = fopen(DIR . '/final-akamai.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }


  private function scrapeCurl ($url)
  {

    $finalUrl = '';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');

    $output        = curl_exec($ch);

    $finalURL      = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    $finalHTTPCODE = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	  $size          = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD) / 1000;
    $headerSize    = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $responseHead  = substr($output, 0, $headerSize);
		$responseBody  = substr($output, $headerSize);

    preg_match_all ('|Location: (.*?)\\r\\n|',$responseHead, $result);
    preg_match_all('|HTTP/\d\.\d\s+(\d+)|',$responseHead, $httpResponse);


    // add called url 
		array_unshift ($result[1], $url);
		$i = 0;

    $httpstat = array();
    $lasthttp = 'error';

    foreach ($httpResponse[1] as $httpStatus) {
    	$httpstat[] = array($result[1][$i], $httpStatus); 
    	$lasthttp = $httpStatus;
    	$i++;
    }

    if (curl_exec($ch) === false) {      
      $httpstat[] = array('ERROR', curl_error($ch));
      $lasthttp = curl_error($ch);
    }

    curl_close($ch);

    $xpath = $this->createDomDocument($responseBody);

    $link = $xpath->query('//link[@rel="canonical"]')->item(0);

		$canonical = '';
    if ($link !== null) {
    	$canonical = $link->getAttribute('href');
    }

    return array($httpstat, $lasthttp, $canonical);

  }

  public function createDomDocument ($html) {
    $dom = new DOMDocument ('1.0');
    @$dom->loadHTML($html);
    $xpath = new DOMXpath($dom);
    return $xpath;
  }

  private function dateYMD ()
  {
    $date = new DateTime();
    $date->add(new DateInterval($this->timeoffset));
    $date = $date->getTimestamp();
    return array(date("Y-m-d", $date), date("H:i:s", $date));
  }

}

new checkRedirects ();

?>
