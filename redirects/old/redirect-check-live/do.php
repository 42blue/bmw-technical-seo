<?php

// 01
// AEM und Server Redirects werden gecheckt 

class checkRedirects
{

  private $result     = array();
  private $timeoffset = 'PT0H';
  private $market     = 'DE';

  public function __construct ()
  {

    $this->readCsv();

    $akamaiHead = array('ruleName','matchURL','result.redirectURL','result.statusCode');
    
    $this->writeResultAkamai($akamaiHead);
    $reminder = array();

    $i=0;
    foreach ($this->basedata as $arr) {

      $redi_origin = $arr[0];
      $redi_target = $arr[1];
      $dataerror = array();
      $data = array();

      $dset = $this->scrapeCurl($redi_origin);

			echo $redi_origin;
			echo PHP_EOL;
			echo $dset[1];
			echo PHP_EOL;

			if ($dset[1] !== '200') {

				foreach ($dset[0] as $key => $redis) {
					$url    = $redis[0];
					$status = $redis[1];
					array_push($dataerror, $url);
					array_push($dataerror, $status);
				}

				$this->writeResultError($dataerror);

			} else {
	      
				foreach ($dset[0] as $key => $redis) {
					$url    = $redis[0];
					$status = $redis[1];
					array_push($data, $url);
					array_push($data, $status);
				}

        $this->writeResult($data);
	
			}

    }

  }

  private function readCsv () {
    $this->basedata = array ();   
    $file = fopen('/var/www/oneproapi/bmw/redirect-check-live/check.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $this->basedata[] = array($line[1], $line[2]);
    }
    fclose($file);
  }

  private function writeResult ($fields) {
    $fp = fopen('/var/www/oneproapi/bmw/redirect-check-live/result.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResultError ($fields) {
    $fp = fopen('/var/www/oneproapi/bmw/redirect-check-live/result-error.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResultAkamai ($fields) {
    $fp = fopen('/var/www/oneproapi/bmw/redirect-check-live/result-akamai.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }


  private function scrapeCurl ($url)
  {

    $finalUrl = '';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');

    $output        = curl_exec($ch);

    $finalURL      = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    $finalHTTPCODE = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	  $size          = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD) / 1000;
    $headerSize    = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $responseHead  = substr($output , 0, $headerSize);

    preg_match_all ('|Location: (.*?)\\r\\n|',$responseHead, $result);
    preg_match_all('|HTTP/\d\.\d\s+(\d+)|',$responseHead, $httpResponse);


    // add called url 
		array_unshift ($result[1], $url);
		$i = 0;

    $httpstat = array();
    $lasthttp = 'error';

    foreach ($httpResponse[1] as $httpStatus) {
    	$httpstat[] = array($result[1][$i], $httpStatus); 
    	$lasthttp = $httpStatus;
    	$i++;
    }

    if (curl_exec($ch) === false) {      
      $httpstat[] = array('ERROR', curl_error($ch));
      $lasthttp = curl_error($ch);
    }

    curl_close($ch);

    return array($httpstat, $lasthttp);

  }

  public function createDomDocument ($html) {
    $dom = new DOMDocument ('1.0');
    @$dom->loadHTML($html);
    $xpath = new DOMXpath($dom);
    return $xpath;
  }

  private function dateYMD ()
  {
    $date = new DateTime();
    $date->add(new DateInterval($this->timeoffset));
    $date = $date->getTimestamp();
    return array(date("Y-m-d", $date), date("H:i:s", $date));
  }

}

new checkRedirects ();

?>
