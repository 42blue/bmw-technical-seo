<?php

// AEM Redirects

define('DIR', dirname(__FILE__));

class checkProof
{

  private $market     = 'DE';

  public function __construct ()
  {

    $basedata = $this->readCsv();

    $this->checkProof($basedata);

  }


  private function checkProof ($basedata) {


    $i=0;
    foreach ($basedata as $set) {

      if (stripos($set[0], $this->market) === FALSE) {
        $arrr = array($set[0],$set[1],$set[2],$set[3]);
        $this->writeResult2($arrr);
        continue;
      }

      $redi_origin = $set[1];
 			$redi_target = $set[2];			

      if ($redi_origin == $redi_target) {
        echo $redi_target;
        echo PHP_EOL;
        continue;
      }

      if (stripos($redi_target, $redi_origin) !== FALSE) {
        echo $redi_target;
        echo PHP_EOL;
        continue;
      }

      if (!isset($seen[$redi_origin])) {
        $i++;
        $arr = array($this->market.$i, $redi_origin, $redi_target, '301');
        $this->writeResult($arr);        
      }

      $seen[$redi_origin] = $redi_origin;

		}

  }

  private function readCsv () {
    $basedata = array ();   
    $file = fopen(DIR . '/final.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $basedata[] = array($line[0], $line[1], $line[2], $line[3]);
    }
    fclose($file);
    return $basedata;
  }

  private function writeResult ($fields) {
    $fp = fopen(DIR . '/result.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResult2 ($fields) {
    $fp = fopen(DIR . '/result2.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

}

new checkProof ();

?>
