<?php

require_once('../technical/base.inc.php');

class overview extends ryteBase {

  public function __construct () {

  	$out = '<!DOCTYPE html><html><head><style>table {width:800px; margin:30px; border-collapse: collapse;} table, th, td {border: 1px solid black; padding:10px;}</style></head><body>';

  	$out .= '<h3>BMW Workfront ZIPs</h3>';
  	$out .= '<ul>';
  	$out .= '<li><a href="../technical/store-workfront/list-http-4xx.zip"> http-4xx.zip </a></li>';
  	$out .= '<li><a href="../technical/store-workfront/list-http-5xx.zip"> http-5xx.zip </a></li>';
  	$out .= '<li><a href="../technical/store-workfront/list-http-301.zip"> http-301.zip </a></li>';
  	$out .= '<li><a href="../technical/store-workfront/list-http-302.zip"> http-302.zip </a></li>';  	  	
		$out .= '</ul>';

    $out .= '<h3>MINI Workfront ZIPs</h3>';
    $out .= '<ul>';
    $out .= '<li><a href="../technical/store-workfront/list-http-4xx-mini.zip"> http-4xx.zip </a></li>';
    $out .= '<li><a href="../technical/store-workfront/list-http-5xx-mini.zip"> http-5xx.zip </a></li>';
    $out .= '<li><a href="../technical/store-workfront/list-http-301-mini.zip"> http-301.zip </a></li>';
    $out .= '<li><a href="../technical/store-workfront/list-http-302-mini.zip"> http-302.zip </a></li>';       
    $out .= '</ul>';

    $out .= '<br /><br />';
    $out .= '<h3>Monthly Reports</h3>';
    
    $out .= '<a href="https://oneproseo.advertising.de/oneproapi/bmw/reporting/admin/sendreport.php">Reports manuell versenden</a>';

    $out .= '<br /><br />';

  	$out .= '<h3>Empfänger BMW Advanced Monthly</h3>';

		$out .= '<table style="border:2px black solid"; "><tr><td><b>Domain(s)</b></td><td><b>Empfänger</b></td></tr>';

  	foreach ($this->sendToTeamsAdvanced as $key => $value) {
	
  		$out .= '<tr><td>';

			foreach ($value['projects'] as $project) {
				$out .= $this->desc[$project][0];
				$out .= '<br />';
			}
			$out .= '</td>';
			$out .= '<td>';
  			$out .= $value['mail'];
  		$out .= '</td></tr>';


  	}

  	$out .= '</table>';


  	$out .= '<h3>Empfänger BMW Basic Monthly</h3>';

		$out .= '<table style="border:2px black solid"; "><tr><td><b>Domain(s)</b></td><td><b>Empfänger</b></td></tr>';

  	foreach ($this->sendToTeamsBasic as $key => $value) {
	
  		$out .= '<tr><td>';

			foreach ($value['projects'] as $project) {
				$out .= $this->desc[$project][0];
				$out .= '<br />';
			}
			$out .= '</td>';
			$out .= '<td>';
  			$out .= $value['mail'];
  		$out .= '</td></tr>';


  	}

  	$out .= '</table>';


  	$out .= '<h3>Empfänger Mini Basic Monthly</h3>';

		$out .= '<table style="border:2px black solid"; "><tr><td><b>Domain(s)</b></td><td><b>Empfänger</b></td></tr>';

  	foreach ($this->sendToTeamsMini as $key => $value) {
	
  		$out .= '<tr><td>';

			foreach ($value['projects'] as $project) {
				$out .= $this->desc[$project][0];
				$out .= '<br />';
			}
			$out .= '</td>';
			$out .= '<td>';
  			$out .= $value['mail'];
  		$out .= '</td></tr>';


  	}

  	$out .= '</table>';


		echo $out;


  }


}

new overview();