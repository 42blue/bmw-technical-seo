<?php

require_once('base.inc.php');
require_once(PATH . '/csv2excel/index.php');

class ryte extends ryteBase {

//public $projects = array('bmw-www.bmw.fr');
//public $listchecks = array('list-http-4xx');

  public function __construct () {

    $this->createDir();

    $this->handleQuerys();

    $this->merge302Redirects();

  }


  private function createDir () {
  	
    $this->newDir = date("Y", strtotime('now')) . date("W", strtotime('+ 2 days'));

    mkdir(PATH.STORE.$this->newDir, 0755);

  }


  private function handleQuerys () {

    foreach ($this->projects as $ryteId) {

      foreach ($this->listchecks as $checkname) {

        $query = file_get_contents(PATH.QUERY.$checkname.'.txt');

        // EXCEPTION FOR JAPANESE CHARACTERS
        if ($ryteId == 'MINI-www.mini.jp' && $checkname == 'list-title-length') {
          $query = file_get_contents(PATH.QUERY.$checkname.'_jp.txt');
        }

        if ($ryteId == 'MINI-www.mini.jp' && $checkname == 'list-md-length') {
          $query = file_get_contents(PATH.QUERY.$checkname.'_jp.txt');
        }

        if ($ryteId == 'bmw-www.bmw.co.jp' && $checkname == 'list-title-length') {
          $query = file_get_contents(PATH.QUERY.$checkname.'_jp.txt');
        }

        if ($ryteId == 'bmw-www.bmw.co.jp' && $checkname == 'list-md-length') {
          $query = file_get_contents(PATH.QUERY.$checkname.'_jp.txt');
        }        
        
        $result = 'null';
        $query = sprintf($query, APIKEY, $ryteId);

        $result = $this->pollApi($query);

        echo $ryteId . ' ' . $checkname . ' - ';
        echo strlen($result);
        echo PHP_EOL;

        $this->saveResult($result, $checkname, $ryteId, $this->checkmeta[$checkname]);

      }

    }

  }


  private function merge302Redirects() {

    $merge = array('list-http-302', 'list-http-302-target');

    $result = array();

    foreach ($this->projects as $ryteId) {

      $result[$ryteId] = array();

      foreach ($merge as $checkname) {

        $data = $this->readCsv($checkname, $ryteId);
        array_push($result[$ryteId], $data);

      }

    }

    foreach ($result as $ryteId => $data) {

      $new = array();

      foreach ($data[0] as $key => $value) {

        $redi_hop = $value[2];

        $trgt = $this->checkForTarget302($redi_hop, $data[1]);

        $new[] = array($value[0], $value[1], $value[2], $value[3], $trgt);

      }

      unset($new[0]);
      unset($new[1]);

      $this->saveResultCSV($new, 'list-http-302', $ryteId, $this->checkmeta['list-http-302']);

    }

  }


  private function saveResult ($result, $checkname, $ryteId, $checkmeta) {

    $data = json_decode($result, true);

    $csvd = array();
    $csvh = array();
    $csvb = array();

    $i = 0;
    $csvd[$i] = array();
    foreach ($checkmeta['todo'] as $key) {
      array_push($csvd[$i], $key);
    }

    $i = 0;
    $csvh[$i] = array();
    foreach ($checkmeta['headlines'] as $key) {
      array_push($csvh[$i], $key);
    }

    $i = 0;
    foreach ($data['result'] as $value) {
      $i++;
      $csvb[$i] = array();
      foreach ($checkmeta['checks'] as $key) {
        array_push($csvb[$i], $value[$key]);
      }
    }

    $csv = array_merge($csvd, $csvh, $csvb);

    $this->writeCsv($csv, $checkname, $ryteId);

  }


  private function saveResultCSV ($result, $checkname, $ryteId, $checkmeta) {

    $csvd = array();
    $csvh = array();
    $csvb = array();

    $i = 0;
    $csvd[$i] = array();
    foreach ($checkmeta['todo'] as $key) {
      array_push($csvd[$i], $key);
    }

    $i = 0;
    $csvh[$i] = array();
    foreach ($checkmeta['headlines'] as $key) {
      array_push($csvh[$i], $key);
    }

    $i = 0;
    foreach ($result as $value) {

      $i++;
      $csvb[$i] = array();
      foreach ($value as $key => $set) {
        array_push($csvb[$i], $set);
      }
    }

    $csv = array_merge($csvd, $csvh, $csvb);

    $this->writeCsv($csv, $checkname, $ryteId);

  }


  private function checkForTarget302 ($redi_hop, $data) {

    foreach ($data as $k => $v) {

      $redi_target = $v[0];

      if ($redi_hop == $redi_target) {
        return $v[2];
      }
      
    }

    return '-';

  }


  private function writeCsv ($fields, $checkname, $ryteId) {

    $fn = PATH.STORE.$this->newDir.'/'.$checkname.'_'.$ryteId.'.csv';

    $fp = fopen($fn, 'w');
    foreach ($fields as $field) {
      fputcsv($fp, $field);
    }

    fclose($fp);

    $xlsx = new BMWCsv2Excel(PATH.STORE.$this->newDir.'/'.$checkname.'_'.$ryteId);
    
  }


  private function readCsv ($checkname, $ryteId) {

    $fn = PATH.STORE.$this->newDir.'/'.$checkname.'_'.$ryteId.'.csv';

    $file = fopen($fn, 'r');

    $arr = array();

    if ($file !== false) {

      while (($line = fgetcsv($file)) !== FALSE) {
        $arr[] = $line;
      }          
      fclose($file);

    } else {

      $arr = false;

    }

    return $arr;
    
  }


  private function pollApi ($query) {

    $apiEndpoint = 'https://api.ryte.com';
    $zoomRoute = '/zoom/json';

    $requestUrl = $apiEndpoint . $zoomRoute;

    $options = array(
        'http' => array(
            'header'  => 'Content-Type: text/json',
            'method'  => 'POST',
            'timeout' => 120, 
            'content' => $query,
        ),
    );

    $context  = stream_context_create($options);

    $result = file_get_contents($requestUrl, false, $context);

    if ($result === FALSE) {  
      echo 'ERROR';
    } 

    return $result;

  }


}

new ryte();