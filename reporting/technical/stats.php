<?php

require_once('base.inc.php');

class ryteReport extends ryteBase {

  public $aggregatechecks = array('list-http-4xx','list-http-5xx','list-title-missing','list-title-duplicate','list-md-missing','list-md-duplicate','list-h1-missing','list-h1-duplicate','list-images-large','list-http-301','list-http-302','list-http-3xx','list-title-length','list-md-length','list-robots-noindex');

	public $projects = array(
		
		'bmw-tahiti-com',
		'bmw-bmw-com-kh',
		'bmw-com-bd',
		'bmw-bmw-abudhabi-com',
		'bmw-bmw-antilles-fr',
		'bmw-bmw-bahrain-com',
		'bmw-bmw-dubai-com',
		'bmw-bmw-eg-com',
		'bmw-bmw-iraq-com',
		'bmw-bmw-jordan-com',
		'bmw-bmw-kuwait-com',
		'bmw-bmw-lao-la',
		'bmw-bmw-lebanon-com',
		'bmw-bmw-oman-com',
		'bmw-bmw-pakistan-com',
		'bmw-bmw-qatar-com',
		'bmw-bmw-saudiarabia-com',
		'bmw-bmw-tunisia-com',
		'bmw-bmw-voli-me',
		'bmw-bmw-yemen-com',
		'bmw-bmw-am',
		'bmw-at',
		'bmw-bmw-az-1',
		'bmw-ba',
		'bmw-bb',
		'bmw-www.bmw.be',
		'bmw-bg',
		'bmw-bs',
		'bmw-by',
		'bmw-bmw-ca',
		'bmw-bmw-cc',
		'bmw-www.bmw.ch',
		'bmw-bmw-cl',
		'bmw-bmw.co.cr',
		'bmw-bmw-co-id',
		'bmw-bmw-co-il',
		'bmw-www.bmw.co.jp',
		'bmw-co-nz',
		'bmw-co-kr',
		'bmw-co-th',
		'bmw-bmw-co-uk',
		'bmw-www.bmw.co.za',
		'bmw-bmw-com-al',
		'bmw-bmw-com-ar',
		'bmw-bmw-com-au',
		'bmw-bmw-com-bn',
		'bmw-bmw-com-bo',
		'bmw-com-br',
		'bmw-com-co',
		'bmw-bmw-com-do',
		'bmw-com-ec',
		'bmw-bmw-com-ge',
		'bmw-bmw-com-gt',
		'bmw-bmw-com-ky',
		'bmw-com-mk',
		'bmw-com-mo',
		'bmw-bmw-com-mx-1',
		'bmw-bmw-com-my',
		'bmw-bmw-com-ni',
		'bmw-bmw-com-pa',
		'bmw-bmw-com-pe-2',
		'bmw-bmw-com-ph',
		'bmw-bmw-com-py',
		'bmw-bmw-com-sv',
		'bmw-com-tr',
		'bmw-bmw-com-uy',
		'bmw-bmw-cw',
		'bmw-bmw-cz',
		'bmw-www.bmw.de',
		'bmw-bmw-dk',
		'bmw-bmw-dz',
		'bmw-bmw-es-1',
		'bmw-bmw-fi',
		'bmw-www.bmw.fr',
		'bmw-bmw-gr',
		'bmw-bmw-hn',
		'bmw-bmw-hr',
		'bmw-bmw-ht',
		'bmw-bmw-hu',
		'bmw-bmw-ie',
		'bmw-bmw-in',
		'bmw-bmw-is',
		'bmw-www.bmw.it',
		'bmw-bmw-kz',
		'bmw-bmw-lc',
		'bmw-bmw-lk',
		'bmw-bmw-lu',
		'bmw-bmw-ly',
		'bmw-bmw-ma',
		'bmw-bmw-md',
		'bmw-bmw-mu',
		'bmw-bmw-nc',
		'bmw-bmw-nl-1',
		'bmw-bmw-no',
		'bmw-bmw-pl',
		'bmw-bmw-ps',
		'bmw-bmw-pt',
		'bmw-bmw-re',
		'bmw-bmw-ro',
		'bmw-bmw-rs',
		'bmw-bmw-ru-1',
		'bmw-bmw-se',
		'bmw-bmw-si',
		'bmw-bmw-sk',
		'bmw-bmw-tt',
		'bmw-bmw-ua',
		'bmw-bmw-vn',
		'bmw-bmwhk-com',
		'bmw-bmwjamaica-com',
		'bmw-bmwmyanmar-com',
		'bmwusa-com',
		'bmw-com-sg',
		'bmw-co-nz',
		'bmw-com-cn',
		'bmw-ee',
		'bmw-lt',
		'bmw-lv',
		'bmw-com-tw',
		'bmw-com-cy',
		'bmw-com-mt');  

  public function __construct () {

    $this->week_now  = '06';
    $this->month_now = 'February';
    $this->year_now  = '2020'; 

    $this->week_last  = '41';
    $this->month_last = 'October';
    $this->year_last  = '2019';

    foreach ($this->sendToTest as $key => $recipient) {

      $data_week_now  = $this->readReports($this->year_now.$this->week_now);
      $data_week_last = $this->readReports($this->year_last.$this->week_last);

      $report = $this->createEmail($data_week_now, $data_week_last);
      
    }

  }



  private function createEmail ($data_week_now, $data_week_last) {

    $out = '';

    $data = array();

    foreach ($data_week_now as $market => $report) {

      $out .= '<table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; font-size: 16px; border: 1px solid #fff; width: 100%;"><tr>';
      $out .= '<tr>';
      $out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:40%; padding: 5px; border-bottom: 5px solid #444444;">' . $this->desc[$market][0] . '</td>';
			$out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';      
			$out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:25%; padding: 5px; border-bottom: 5px solid #444444;">'. $this->month_last .' / ' . $this->year_last . '</td>';
			$out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';
      $out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:100px; padding: 5px; border-bottom: 5px solid #444444;">+/-</td>';     
      $out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>'."\n";        
			$out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:25%; padding: 5px; border-bottom: 5px solid #444444;">'.$this->month_now .' / ' . $this->year_now . '</td>';
      $out .= '</tr>';

      foreach ($report as $check => $metric) {

        $errors_week_now  = $metric - 2;
        $errors_week_last = $data_week_last[$market][$check] - 2;

      	if (isset($data[$check])) {
      		$data[$check]['now'] = $data[$check]['now'] + $errors_week_now;
      		$data[$check]['then'] = $data[$check]['then'] + $errors_week_last;
      	} else {
      		$data[$check] = array('now' => $errors_week_now, 'then' => $errors_week_last);      		
      	}

        $out .= '<tr>';    
        $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' .$check.'</td>'."\n";
				$out .= '<td style="width:5px; background-color:#444444;"></td>';
        $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' .$errors_week_last .'</td>'."\n";
				$out .= '<td style="width:5px; background-color:#444444;"></td>';
        $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;"></td>'."\n";
        $out .= '<td style="width:5px; background-color:#444444;"></td>';        
        $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' .$errors_week_now . '</td>'."\n";

        $out .= '</tr>';

      }

      $out .= '</table><br /><br />';
      $out .= '<div style="background-color:#444444; height:10px; "></div>';

    }



    $out = '<table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; font-size: 16px; border: 1px solid #fff; width: 100%;"><tr>';
    $out .= '<tr>';
    $out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:40%; padding: 5px; border-bottom: 5px solid #444444;">Check</td>';
		$out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';      
		$out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:25%; padding: 5px; border-bottom: 5px solid #444444;">'. $this->month_last .' / ' . $this->year_last . '</td>';
		$out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';
    $out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:100px; padding: 5px; border-bottom: 5px solid #444444;">+/-</td>';     
    $out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>'."\n";        
		$out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:25%; padding: 5px; border-bottom: 5px solid #444444;">'.$this->month_now .' / ' . $this->year_now . '</td>';
    $out .= '</tr>';
    

    foreach ($data as $check => $set) {

    	$change = $set['now'] - $set['then'];
    	if ($change > 9) {
    		$change = '+'.$change;
    	}

      $out .= '<tr>';    
      $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' .str_replace('list-', '', $check).'</td>'."\n";
			$out .= '<td style="width:5px; background-color:#444444;"></td>';
      $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' .$set['then'] .'</td>'."\n";
			$out .= '<td style="width:5px; background-color:#444444;"></td>';
      $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' .$change. '</td>'."\n";
      $out .= '<td style="width:5px; background-color:#444444;"></td>';        
      $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' .$set['now'] .'</td>'."\n";

      $out .= '</tr>';

    }

    $out .= '</table><br /><br />';

    echo $out;

  }



  private function readReports ($week) {

    $result = array();

    foreach ($this->projects as $ryteId) {

      foreach ($this->aggregatechecks as $checkname) {

        $fn = PATH.STORE.$week.'/'.$checkname.'_'.$ryteId.'.csv';

        $file = fopen($fn, 'r');

        $arr = array();

        if ($file !== false) {

          while (($line = fgetcsv($file)) !== FALSE) {
            $arr[] = $line[0];
          }          
          fclose($file);

        } else {

          $arr = false;

        }

        $result[$ryteId][$checkname] = count($arr);

      }

    }

    return $result;

  }


}

new ryteReport();