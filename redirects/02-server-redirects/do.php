<?php

// AEM Redirects

define('DIR', dirname(__FILE__));

class checkAEMRedirects
{

  private $market     = 'DE';

  public function __construct ()
  {

    $basedata = $this->readCsv();

    $this->scrapeURLs($basedata);

  }


  private function scrapeURLs ($new) {

    $this->writeResult(array('Redi Source', 'Redi Target', 'Amount of redirects', 'Final URL', 'Final URL Canonical', 'Canonical Match'));
    $this->writeResultError(array('Server Source', 'Server Target', 'Amount of redirects', 'Final URL', 'Final URL HTTP Status'));
    $this->writeResultAkamai(array('ruleName','matchURL','result.redirectURL','result.statusCode'));    

    $i = 0;
    foreach ($new as $set) {

			$redi_origin = $set[0];
			$redi_target = $set[1];			

      if (empty($redi_origin)) {
        continue;
      }

      if (substr( $redi_origin, 0, 4 ) !== 'http') {
        continue;
      }

      $dset = $this->scrapeCurl($redi_origin);

      $redi_chain = $dset['redis'];
      $last_http  = $dset['finalhttp'];
      $canonical  = $dset['canonical'];

			echo $redi_origin . ' ' . $last_http;
			echo PHP_EOL;

			$dataerror = $set;
			$datagood  = $set;			
      $dataakamai = array();


			if ($last_http !== '200') {

				foreach ($redi_chain as $key => $redis) {
					$url    = $redis[0];
					$status = $redis[1];
					$lasturl = $url;
					$laststatus = $status;
				}

				$redi_hops = count($redi_chain) - 1;
				array_push($dataerror, $redi_hops);
				array_push($dataerror, $lasturl);
				array_push($dataerror, $laststatus);

				$this->writeResultError($dataerror);

			} else {

				foreach ($redi_chain as $key => $redis) {
					$url     = $redis[0];
					$status  = $redis[1];
          $lasturl = $url;
				}

				$redi_hops = count($redi_chain) - 1;
				array_push($datagood, $redi_hops);
				array_push($datagood, $lasturl);

				if (empty($canonical)) {
					array_push($datagood, 'none');					
				} else {
					array_push($datagood, $canonical);					
				}

				if ($lasturl != $canonical) {
					array_push($datagood, 'false');
				} else {
					array_push($datagood, 'true');
				}

        $this->writeResult($datagood);

        // AKAMAI
        $i++;
        array_push($dataakamai, $this->market.$i);
        array_push($dataakamai, $redi_origin);
        if (empty($canonical)) {
          array_push($dataakamai, $lasturl);
        } else {
          array_push($dataakamai, $canonical);
        }        
        array_push($dataakamai, '301');

        $this->writeResultAkamai($dataakamai);        

			}

		}

  }

  private function readCsv () {
    $basedata = array ();   
    $file = fopen(DIR . '/check.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $basedata[] = array($line[0], $line[1]);
    }
    fclose($file);
    return $basedata;
  }

  private function writeResult ($fields) {
    $fp = fopen(DIR . '/result.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResultError ($fields) {
    $fp = fopen(DIR . '/result-error.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResultAkamai ($fields) {
    $fp = fopen(DIR . '/result-akamai.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }


  private function scrapeCurl ($url)
  {

    $finalUrl = '';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');

    $output        = curl_exec($ch);

    $finalURL      = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    $finalHTTPCODE = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	  $size          = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD) / 1000;
    $headerSize    = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $responseHead  = substr($output, 0, $headerSize);
		$responseBody  = substr($output, $headerSize);


    preg_match_all ('|Location: (.*?)\\r\\n|',$responseHead, $result);
    preg_match_all('|HTTP/\d\.\d\s+(\d+)|',$responseHead, $httpResponse);


    // add called url 
		array_unshift ($result[1], $url);
		$i = 0;

    $httpstat = array();
    $lasthttp = 'error';

    foreach ($httpResponse[1] as $httpStatus) {
    	$httpstat[] = array($result[1][$i], $httpStatus); 
    	$lasthttp = $httpStatus;
    	$i++;
    }

    if (curl_exec($ch) === false) {      
      $httpstat[] = array('ERROR', curl_error($ch));
      $lasthttp = curl_error($ch);
    }

    curl_close($ch);

    $xpath = $this->createDomDocument($responseBody);

    $link = $xpath->query('//link[@rel="canonical"]')->item(0);

		$canonical = '';
    if ($link !== null) {
    	$canonical = $link->getAttribute('href');
    }

    return array('redis' => $httpstat, 'finalhttp' => $lasthttp, 'canonical' => $canonical);


  }

  public function createDomDocument ($html) {
    $dom = new DOMDocument ('1.0');
    @$dom->loadHTML($html);
    $xpath = new DOMXpath($dom);
    return $xpath;
  }

}

new checkAEMRedirects ();

?>
