Sitemap: http://www.mini.co.nz/sitemap.xml
User-agent: *
Disallow: /request-a-test-drive/?*
Disallow: /download-ebrochure/?*

Disallow: /soc-mini-convertible/?*
Disallow: /mini-hatch-register-your-interest/?*