<?php

require_once('base.inc.php');
require_once(PATH . '/csv2excel/index.php');

class ryte extends ryteBase {

	//public $projects = array('MINI-www.mini.jp', 'bmw-www.bmw.co.jp', 'bmw-www.bmw.ch');
  public $listchecks = array('list-urls-all');

  public function __construct () {

    $this->createDir();

    $this->handleQuerys();

  }


  private function createDir () {
  	
    $this->newDir = date("Y", strtotime('now')) . date("W", strtotime('+ 2 days'));

    mkdir(PATH.STORE.'custom', 0755);

  }


  private function handleQuerys () {

    foreach ($this->projects as $ryteId) {

      foreach ($this->listchecks as $checkname) {

        $query = file_get_contents(PATH.QUERY.'list-urls-all'.'.txt');
                
        $result = 'null';
        $query = sprintf($query, APIKEY, $ryteId);

        $result = $this->pollApi($query);

        echo $ryteId . ' ' . $checkname . ' - ';
        echo strlen($result);
        echo PHP_EOL;

        $this->saveResult($result, $checkname, $ryteId);

      }

    }

  }



  private function saveResult ($result, $checkname, $ryteId) {

    $data = json_decode($result, true);

    $csv = array();

    foreach ($data['result'] as $value) {

      $csv[] = array($value['url']);

    }

    $this->writeCsv($csv, $checkname, $ryteId);

  }


  private function writeCsv ($fields, $checkname, $ryteId) {

    $fn = PATH.STORE.'custom/'.$checkname.'_'.$ryteId.'.csv';

    $fp = fopen($fn, 'w');
    foreach ($fields as $field) {
      fputcsv($fp, $field);
    }

    fclose($fp);
    
  }


  private function pollApi ($query) {

    $apiEndpoint = 'https://api.ryte.com';
    $zoomRoute = '/zoom/json';

    $requestUrl = $apiEndpoint . $zoomRoute;

    $options = array(
        'http' => array(
            'header'  => 'Content-Type: text/json',
            'method'  => 'POST',
            'timeout' => 120, 
            'content' => $query,
        ),
    );

    $context  = stream_context_create($options);

    $result = file_get_contents($requestUrl, false, $context);

    if ($result === FALSE) {  
      echo 'ERROR';
    } 

    return $result;

  }


}

new ryte();