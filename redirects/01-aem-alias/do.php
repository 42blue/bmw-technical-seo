<?php

// AEM Redirects

define('DIR', dirname(__FILE__));

class checkAEMRedirects
{

  private $market     = 'DE';

  public function __construct ()
  {

    $basedata = $this->readCsv();
    $this->sc_urls  = $this->readCsvSC();

    $new = $this->createProperURLs($basedata);

    $this->scrapeURLs($new);

  }


  private function scrapeURLs ($new) {

    $this->writeResult(array('AEM Path', 'AEM Title', 'AEM Redi', 'AEM Alias', 'Computed Redirect', 'Amount of redirects', 'Final URL', 'Final URL Canonical', 'Canonical Match', 'Computed Redirect found in Search Console'));
    $this->writeResultError(array('AEM Path', 'AEM Title', 'AEM Redi', 'AEM Alias', 'Computed Redirect', 'Amount of redirects', 'Final URL', 'Final URL HTTP Status', 'Computed Redirect found in Search Console'));
    $this->writeResultAkamai(array('ruleName','matchURL','result.redirectURL','result.statusCode'));    

    $i = 0;
    foreach ($new as $aemset) {

			$aem_path  	 = $aemset[0];
			$aem_title 	 = $aemset[1];
			$aem_redi  	 = $aemset[2];
			$aem_alias 	 = $aemset[3];
			$redi_origin = $aemset[4];


      if (empty($redi_origin)) {
        continue;
      }

      if (substr( $redi_origin, 0, 4 ) !== 'http') {
        continue;
      }

      $dset = $this->scrapeCurl($redi_origin);

      $redi_chain = $dset['redis'];
      $last_http  = $dset['finalhttp'];
      $canonical  = $dset['canonical'];

			echo $redi_origin . ' ' . $last_http;
			echo PHP_EOL;

			$dataerror = $aemset;
			$datagood  = $aemset;			
      $dataakamai = array();


			if ($last_http !== '200') {

				foreach ($redi_chain as $key => $redis) {
					$url    = $redis[0];
					$status = $redis[1];
					$lasturl = $url;
					$laststatus = $status;
				}

				$redi_hops = count($redi_chain) - 1;
				array_push($dataerror, $redi_hops);
				array_push($dataerror, $lasturl);
				array_push($dataerror, $laststatus);

				if (isset($this->sc_urls[$redi_origin])) {
					array_push($dataerror, 'true');
				} else {
					array_push($dataerror, 'false');
				}

				$this->writeResultError($dataerror);

			} else {

				foreach ($redi_chain as $key => $redis) {
					$url     = $redis[0];
					$status  = $redis[1];
          $lasturl = $url;
				}

				$redi_hops = count($redi_chain) - 1;
				array_push($datagood, $redi_hops);
				array_push($datagood, $lasturl);

				if (empty($canonical)) {
					array_push($datagood, 'none');					
				} else {
					array_push($datagood, $canonical);					
				}

				if ($lasturl != $canonical) {
					array_push($datagood, 'false');
				} else {
					array_push($datagood, 'true');
				}

				if (isset($this->sc_urls[$redi_origin])) {
					array_push($datagood, 'true');
				} else {
					array_push($datagood, 'false');
				}

        $this->writeResult($datagood);

        // AKAMAI
        $i++;
        array_push($dataakamai, $this->market.$i);
        array_push($dataakamai, $redi_origin);
        if (empty($canonical)) {
          array_push($dataakamai, $lasturl);
        } else {
          array_push($dataakamai, $canonical);
        }        
        array_push($dataakamai, '301');

        $this->writeResultAkamai($dataakamai);        

			}

		}

  }



  private function createProperURLs ($basedata) {

		$newbase = array();
    $cracked = array();
    $alias   = array();

    foreach ($basedata as $arr) {

      $aem_path   = $arr[0];
      $aem_title  = $arr[1];
      $aem_target = $arr[2];
      $aem_alias  = $arr[3];

      if (stripos($aem_path, 'marketDE') == false) {
        continue;
      }

      if (stripos($aem_path, '/jcr:content') !== FALSE) {
        $final_path = str_replace('/jcr:content', '', $aem_path);        
      }

      if (stripos($final_path, '/content/bmw/marketDE/bmw_de/de_DE/') !== FALSE) {
        $final_path = str_replace('/content/bmw/marketDE/bmw_de/de_DE/', 'https://www.bmw.de/de/', $final_path);        
      }

      $newbase[] = $arr;
      $alias[]   = $aem_alias;
      $cracked[] = explode('/', $final_path);

    }

    // PREPARE DATA 
    $i=0;    
    $replacer = array();
    foreach ($cracked as $key => $crack) {

      $replacement_position = count($crack) - 1;
      $replacer[] = array($crack[$replacement_position], $alias[$i], $replacement_position, $crack);

      $i++;

    }


    // REPLACE URL PARTS
    $x=0;
    foreach ($cracked as $key => $parts) {


//4-8
      foreach ($replacer as $pos => $repl) {

				$result = array_diff($repl[3], $parts);
//echo count($result);
//echo PHP_EOL;
        $old = $repl[0];
        $new = $repl[1];
        $atp = $repl[2];

        if (isset($parts[$atp]) && count($result) < 1) {

          if ($parts[$atp] == $old) {
            $cracked[$x][$atp] = $new;
          }

        }

      }

      $x++;

    }

    // ASSEMBLE NEW URLS
    $new = array();
    foreach ($cracked as $key => $value) {
      $url = '';
      foreach ($value as $part) {
        $url .= $part . '/';
      }
      $final = substr($url, 0, -1).'.html';
      $new[] = $final;      
    }

    // MERGE WITH EXISTING DATA
		$c=0;
    foreach ($new as $url) {
    	array_push($newbase[$c], $url);
    	$c++;
    }

    return $newbase;

  }

  private function readCsv () {
    $basedata = array ();   
    $file = fopen(DIR . '/check.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $basedata[] = array($line[0], $line[1], $line[2], $line[3]);
    }
    fclose($file);
    return $basedata;
  }

  private function readCsvSC () {
    $basedata = array ();   
    $file = fopen(DIR . '/check-sc.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $basedata[$line[0]] = array($line[0], $line[1], $line[2], $line[3]);
    }
    fclose($file);
    return $basedata;
  }

  private function writeResult ($fields) {
    $fp = fopen(DIR . '/result.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResultError ($fields) {
    $fp = fopen(DIR . '/result-error.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResultAkamai ($fields) {
    $fp = fopen(DIR . '/result-akamai.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }


  private function scrapeCurl ($url)
  {

    $finalUrl = '';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');

    $output        = curl_exec($ch);

    $finalURL      = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    $finalHTTPCODE = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	  $size          = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD) / 1000;
    $headerSize    = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $responseHead  = substr($output, 0, $headerSize);
		$responseBody  = substr($output, $headerSize);


    preg_match_all ('|Location: (.*?)\\r\\n|',$responseHead, $result);
    preg_match_all('|HTTP/\d\.\d\s+(\d+)|',$responseHead, $httpResponse);


    // add called url 
		array_unshift ($result[1], $url);
		$i = 0;

    $httpstat = array();
    $lasthttp = 'error';

    foreach ($httpResponse[1] as $httpStatus) {
    	$httpstat[] = array($result[1][$i], $httpStatus); 
    	$lasthttp = $httpStatus;
    	$i++;
    }

    if (curl_exec($ch) === false) {      
      $httpstat[] = array('ERROR', curl_error($ch));
      $lasthttp = curl_error($ch);
    }

    curl_close($ch);

    $xpath = $this->createDomDocument($responseBody);

    $link = $xpath->query('//link[@rel="canonical"]')->item(0);

		$canonical = '';
    if ($link !== null) {
    	$canonical = $link->getAttribute('href');
    }

    return array('redis' => $httpstat, 'finalhttp' => $lasthttp, 'canonical' => $canonical);


  }

  public function createDomDocument ($html) {
    $dom = new DOMDocument ('1.0');
    @$dom->loadHTML($html);
    $xpath = new DOMXpath($dom);
    return $xpath;
  }

}

new checkAEMRedirects ();

?>
