<?php

// AEM Redirects

define('DIR', dirname(__FILE__));

class checkAEMRedirects
{

  private $market     = 'DE';

  public function __construct ()
  {

    $basedata = $this->readCsv();
    $newbase  = $this->createProperURLs($basedata);

    $this->scrapeURLs($newbase);

  }


  private function scrapeURLs ($new) {

    $this->writeResult(array('Market','AEM Path', 'AEM Title', 'AEM Redirect Target', 'AEM Alias', 'Computed Redirect', 'Amount of redirects', 'Final URL', 'Final URL Canonical', 'Canonical Match'));
    $this->writeResultError(array('Market','AEM Path', 'AEM Title', 'AEM Redirect Target', 'AEM Alias', 'Computed Redirect', 'Final URL', 'Final URL HTTP Status / Error'));
    $this->writeResultAkamai(array('ruleName','matchURL','result.redirectURL','result.statusCode'));    

    $i = 0;
    foreach ($new as $aemset) {

      $market      = $aemset[0];
			$aem_path  	 = $aemset[1];
			$aem_title 	 = $aemset[2];
			$aem_redi  	 = $aemset[3];
			$aem_alias 	 = $aemset[4];
			$redi_origin = $aemset[5];


      if (empty($redi_origin)) {
        continue;
      }

      if (substr( $redi_origin, 0, 4 ) !== 'http') {
        continue;
      }

      $dset = $this->scrapeCurl($redi_origin);

      $redi_chain = $dset['redis'];
      $last_http  = $dset['finalhttp'];
      $canonical  = $dset['canonical'];
      $lasturl    = $dset['finalurl'];

			echo $redi_origin . ' ' . $last_http;
			echo PHP_EOL;

			$dataerror = $aemset;
			$datagood  = $aemset;			
      $dataakamai = array();


			if ($last_http !== '200') {

				array_push($dataerror, $lasturl);
				array_push($dataerror, $last_http);

				$this->writeResultError($dataerror);

			} else {

				foreach ($redi_chain as $key => $redis) {
					$url     = $redis[0];
					$status  = $redis[1];
				}

				$redi_hops = count($redi_chain) - 1;

				array_push($datagood, $redi_hops);
				array_push($datagood, $lasturl);

				if (empty($canonical)) {
					array_push($datagood, 'none');					
				} else {
					array_push($datagood, $canonical);					
				}

				if ($lasturl != $canonical) {
					array_push($datagood, 'false');
				} else {
					array_push($datagood, 'true');
				}

        $this->writeResult($datagood);

        // AKAMAI
        if ($redi_hops == 0) {
          continue;
        }
        $i++;
        array_push($dataakamai, $market.$i);
        array_push($dataakamai, $redi_origin);
        if (empty($canonical)) {
          array_push($dataakamai, $lasturl);
        } else {
          array_push($dataakamai, $canonical);
        }        
        array_push($dataakamai, '301');

        $this->writeResultAkamai($dataakamai);        

			}

		}

  }



  private function createProperURLs ($basedata) {

		$newbase = array();

    foreach ($basedata as $arr) {

    	$fullurl    = 'false'; 
      $aem_path   = $arr[0];
      $aem_title  = $arr[1];
      $aem_target = $arr[2];
      $aem_alias  = $arr[3];


      $aem_url = $aem_path;
			if (!empty($aem_alias)) {
				$aem_url = $aem_alias;
			}

      // REMOVE AEM PATH
      $ex = explode('/', $aem_url);
      unset($ex[0]); // /
      unset($ex[1]); // content/
      unset($ex[2]); // MINI/
      $market = str_replace('market', '', $ex[3]);
      unset($ex[3]); // marketAT/
      if (isset($ex[4])) {
        $ex[4] = str_replace('_', '.', $ex[4]);        
      }

      $fullurl = 'https://www.' . implode('/', $ex);

      $newbase[] = array($market, $aem_path, $aem_title, $aem_target, $aem_alias, $fullurl);

    }

    return $newbase;

  }

  private function readCsv () {
    $basedata = array ();   
    $file = fopen(DIR . '/input.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $basedata[] = array($line[0], $line[1], $line[2], $line[3]);
    }
    fclose($file);
    return $basedata;
  }

  private function writeResult ($fields) {
    $fp = fopen(DIR . '/result.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResultError ($fields) {
    $fp = fopen(DIR . '/result-error.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResultAkamai ($fields) {
    $fp = fopen(DIR . '/result-akamai.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }


  private function scrapeCurl ($url)
  {

    $finalUrl = '';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');

    $output        = curl_exec($ch);

    $finalURL      = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    $finalHTTPCODE = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	  $size          = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD) / 1000;
    $headerSize    = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $responseHead  = substr($output, 0, $headerSize);
		$responseBody  = substr($output, $headerSize);


    preg_match_all ('|Location: (.*?)\\r\\n|',$responseHead, $result);
    preg_match_all('|HTTP/\d\.\d\s+(\d+)|',$responseHead, $httpResponse);


    // add called url 
		array_unshift ($result[1], $url);
		$i = 0;

    $httpstat = array();
    $lasthttp = 'error';

    foreach ($httpResponse[1] as $httpStatus) {
    	$httpstat[] = array($result[1][$i], $httpStatus); 
    	$lasthttp = $httpStatus;
    	$i++;
    }

    if (curl_exec($ch) === false) {      
      $httpstat[] = array('ERROR', curl_error($ch));
      $lasthttp = curl_error($ch);
    }

    curl_close($ch);

    $xpath = $this->createDomDocument($responseBody);

    $link = $xpath->query('//link[@rel="canonical"]')->item(0);

		$canonical = '';
    if ($link !== null) {
    	$canonical = $link->getAttribute('href');
    }

    return array('redis' => $httpstat, 'finalhttp' => $lasthttp, 'canonical' => $canonical, 'finalurl' => $finalURL);


  }

  public function createDomDocument ($html) {
    $dom = new DOMDocument ('1.0');
    @$dom->loadHTML($html);
    $xpath = new DOMXpath($dom);
    return $xpath;
  }

}

new checkAEMRedirects ();

?>
