<?php

// 02
// Neue Listen von BMW nach dem 01 Check prüfen und konvertieren


class checkRedirects
{

  private $result     = array();
  private $timeoffset = 'PT0H';
  private $market     = 'DE';

  public function __construct ()
  {

    $this->readCsv();

    $i = 1362;
    foreach ($this->basedata as $arr) {

      echo $redi_origin     = trim($arr[0]);
      echo PHP_EOL;
      $redi_target     = trim($arr[1]);

			if (empty($redi_target)) {
			  $data404 = array($redi_origin,'404');
        $this->writeResult404($data404);
			} else {

				$stat = $this->scrapeCurl($redi_target);

				if ($stat[1] == '200') {

					$dataakamai = array($this->market.$i++, $redi_origin, $redi_target, '301');
          $this->writeResultAkamai($dataakamai);          

				} else {

					$dataerror = array($redi_origin, $redi_target, $stat[1]);
					$this->writeResultError($dataerror);

				}

			}


    }

  }

  private function readCsv () {
    $this->basedata = array ();   
    $file = fopen('/var/www/oneproapi/bmw/redirect-convert/check.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $this->basedata[] = array($line[0], $line[1]);
    }
    fclose($file);
  }

  private function writeResultError ($fields) {
    $fp = fopen('/var/www/oneproapi/bmw/redirect-convert/result-error.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResult404 ($fields) {
    $fp = fopen('/var/www/oneproapi/bmw/redirect-convert/result-404.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResultAkamai ($fields) {
    $fp = fopen('/var/www/oneproapi/bmw/redirect-convert/result-akamai.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }


  private function scrapeCurl ($url)
  {

    $finalUrl = '';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');

    $output        = curl_exec($ch);

    $finalURL      = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    $finalHTTPCODE = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	  $size          = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD) / 1000;
    $headerSize    = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $responseHead  = substr($output , 0, $headerSize);

    preg_match_all ('|Location: (.*?)\\r\\n|',$responseHead, $result);
    preg_match_all('|HTTP/\d\.\d\s+(\d+)|',$responseHead, $httpResponse);


    // add called url 
		array_unshift ($result[1], $url);
		$i = 0;

    $httpstat = array();
    $lasthttp = 'error';

    foreach ($httpResponse[1] as $httpStatus) {
    	$httpstat[] = array($result[1][$i], $httpStatus); 
    	$lasthttp = $httpStatus;
    	$i++;
    }

    if (curl_exec($ch) === false) {      
      $httpstat[] = array('ERROR', curl_error($ch));
      $lasthttp = curl_error($ch);
    }

    curl_close($ch);

    return array($httpstat, $lasthttp);

  }

  public function createDomDocument ($html) {
    $dom = new DOMDocument ('1.0');
    @$dom->loadHTML($html);
    $xpath = new DOMXpath($dom);
    return $xpath;
  }

  private function dateYMD ()
  {
    $date = new DateTime();
    $date->add(new DateInterval($this->timeoffset));
    $date = $date->getTimestamp();
    return array(date("Y-m-d", $date), date("H:i:s", $date));
  }

}

new checkRedirects ();

?>
