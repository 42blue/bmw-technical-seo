<?php


/*
Ryte KPI API

https://api.ryte.com/kpis/v1/
https://api.ryte.com/kpis/v1/#!/Zoom45KPIs/queryOnpageScores

apiKey 58320f65ee73b235249daf75de4ab71f

Project Hash
p12cb224d68d39762785fa75cc1d37d0

*/

error_reporting(E_ALL);

define('PATH', dirname(__FILE__));
define('QUERY', '/query/');
define('ORIGIN', '/origin/');
define('STOREWORKFRONT', '/store-workfront/');
define('STOREMANAGEMENT', '/store-management/');
define('STORE', '/store/');
define('WWW', 'https://oneproseo.advertising.de/oneproapi/bmw/reporting/technical/store/');
define('APIKEY', '58320f65ee73b235249daf75de4ab71f');

class ryteBase {


  public $sendToTest   = array(

    'aggregate'         => array(
      'mail'     => 'matthias.hotz@diva-e.com',
      'projects' => array('bmw-bmw-lu', 'bmw-www.bmw.de')
    )

  );


  public $sendToTeamsAdvancedWeekly   = array(

    'bmw-www.bmw.de' 		=> array(
    	'mail' 		 => 'Manuel.Simelka@bmw.com, Michael.SC.Schulz@bmw.de, matthias.hotz@diva-e.com', 
    	'projects' => array('bmw-www.bmw.de')
    ),
  
  );


  public $sendToTeamsAdvanced   = array(

    'bmw-www.bmw.de' 		=> array(
    	'mail' 		 => 'Manuel.Simelka@bmw.com, Michael.SC.Schulz@bmw.de', 
    	'projects' => array('bmw-www.bmw.de')
    ),
    
    'bmw-www.bmw.be' 		=> array(
    	'mail'     => 'Gert.Vanhoegaerden@bmw.be, steven.witpas@bmw.be', 
    	'projects' => array('bmw-www.bmw.be', 'bmw-bmw-lu')
    ),

    'bmw-www.bmw.fr' 		=> array(
    	'mail'     => 'Elise.Fauchon@bmw.fr, Adrien.Filipe@bmw.fr, Alain.Barbotte@partner.bmw.fr',
    	'projects' => array('bmw-www.bmw.fr')
    ),
    
    'bmw-www.bmw.it' 		=> array(
    	'mail'     => 'Fabrizio.Farsetta@bmw.it',
    	'projects' => array('bmw-www.bmw.it')
    ),
    
    'bmw-www.bmw.ch' 		=> array(
    	'mail'     => 'Sybille.Maier@bmw.ch , Sergey.SF.Fedorenko@partner.bmw.ch',
    	'projects' => array('bmw-www.bmw.ch')
    ),
    
    'bmw-www.bmw.co.jp' => array(
    	'mail'     => 'Vroni-Friederike.Shiohara@bmw.co.jp, hirotaka.nishino@imjp.co.jp, toshiki.kurihara@partner.bmw.co.jp, Tomoko.Inoue@bmw.co.jp, Kota.Kosugi@partner.bmwgroup.com',
    	'projects' => array('bmw-www.bmw.co.jp')
    ),
    
    'bmw-bmw-co-id' => array(
      'mail'     => 'Anastasha.Evasari-Yanuar@bmw.co.id', 
      'projects' => array('bmw-bmw-co-id')
    ),    
    
    'aggregate'         => array(
    	'mail'     => 'Christine.von-Marsoner@bmw.com, Cora.Guentert@bmw.de, bmw-seo@diva-e.com',
    	'projects' => array('bmw-www.bmw.de', 'bmw-www.bmw.be', 'bmw-bmw-lu', 'bmw-www.bmw.fr', 'bmw-www.bmw.it', 'bmw-www.bmw.ch', 'bmw-www.bmw.co.jp', 'bmw-bmw-co-id')
    ),
    
    'admin'             => array(
    	'mail'     => 'm.hotz@advertising.de', 
    	'projects' => array('bmw-www.bmw.de', 'bmw-www.bmw.be', 'bmw-bmw-lu', 'bmw-www.bmw.fr', 'bmw-www.bmw.it', 'bmw-www.bmw.ch', 'bmw-www.bmw.co.jp', 'bmw-bmw-co-id')
    )

  );


  public $sendToTeamsBasic   = array(

    'Austria' => array(
      'mail'     => 'Michael.Schnoell@bmwgroup.at, sarah.forsthuber@bmwgroup.at, matthias.hotz@diva-e.com', 
      'projects' => array('bmw-at')
    ),
    'Australia' => array(
      'mail'     => 'Graham.Abrey@bmw.com.au, Daniel.J.Fraser@bmw.com.au, Loan.Tran@bmwfinance.com.au, Tony.Sesto@bmw.com.au', 
      'projects' => array('bmw-bmw-com-au')
    ),
    'South East Asia' => array(
      'mail'     => 'Sok-Hoon.Koo@bmwasia.com, peck-kheng.ho@bmwasia.com', 
      'projects' => array('bmw-bmw-cc', 'bmw-bmw-com-bn', 'bmw-bmw-com-kh', 'bmw-bmw-com-ph', 'bmw-bmw-lao-la', 'bmw-bmw-lk', 'bmw-bmw-nc', 'bmw-bmwmyanmar-com', 'bmw-bmw-vn', 'bmw-com-sg', 'bmw-com-bd')
    ),
    'Brazil' => array(
      'mail'     => 'Jorge.Junior@bmw.com.br, felipe.oliveira@ogilvy.com, pedro.baldi@ogilvy.com, Priscila.Bartolomeo@bmw.com.br', 
      'projects' => array('bmw-com-br')
    ),
    'Canada' => array(
      'mail'     => 'Connor.Kennedy@bmwgroup.ca', 
      'projects' => array('bmw-bmw-ca')
    ),
    'China' => array(
      'mail'     => 'ferrol.ji@bmw.com, judy.fan@bmw.com', 
      'projects' => array('bmw-com-cn')
    ),
    'Spain' => array(
      'mail'     => 'MiguelAngel.Godoy@bmw.es, Ignacio.Pereyra@bmw.es, esther.coronado@bmw.es, ccid@deloittedigital.es, cbellido@deloittedigital.es, belen.valero@bmw.es, Ana.Llinas@bmw.es ', 
      'projects' => array('bmw-bmw-es-1')
    ),
    'Hong-Kong' => array(
      'mail'     => 'darren.wu@bmwhk.com, Bonnie.Kuo@partner.bmw.com', 
      'projects' => array('bmw-bmwhk-com')
    ),
    'Croatia' => array(
      'mail'     => 'kristina.krpan@tomic.hr, tino.ostojic@tomic.hr', 
      'projects' => array('bmw-bmw-hr')
    ),
    'Hungary' => array(
      'mail'     => 'bianka.nagy@bmw.hu, zsanett.badaczi@partner.bmw.hu', 
      'projects' => array('bmw-bmw-hu')
    ),
    'Ireland' => array(
      'mail'     => 'Muireann.Bunting@bmw.ie, paul.osullivan@bmw.ie, DavidRandell@oliver.agency', 
      'projects' => array('bmw-bmw-ie')
    ),
    'India' => array(
      'mail'     => 'Sumedha.Singh@bmw.in, inderpreet.Sethi@bmw.in, Srihitha.Narra@hogarthww.com', 
      'projects' => array('bmw-bmw-in')
    ),
    'Korea' => array(
      'mail'     => 'Jeonghun.Ha@bmw.co.kr', 
      'projects' => array('bmw-co-kr')
    ),
    'Lithuania, Latvia, Estonia' => array(
      'mail'     => 'paul.bako@bmw.se, justina.ignatjevaite@bmw.lt, Marie.Dellbrant@bmw.se, jana.garanca@inchcape.lv, laura.belska@bmw.lv, maren.uibo@inchcape.ee', 
      'projects' => array('bmw-ee', 'bmw-lt', 'bmw-lv')
    ),
    'Middle East' => array(
      'mail'     => 'Fahad.Mughal@bmw.com', 
      'projects' => array('bmw-bmw-abudhabi-com', 'bmw-bmw-bahrain-com', 'bmw-bmw-dubai-com', 'bmw-bmw-iraq-com', 'bmw-bmw-jordan-com', 'bmw-bmw-kuwait-com', 'bmw-bmw-lebanon-com', 'bmw-bmw-oman-com', 'bmw-bmw-pakistan-com', 'bmw-bmw-qatar-com', 'bmw-bmw-saudiarabia-com', 'bmw-bmw-tunisia-com', 'bmw-bmw-yemen-com', 'bmw-bmw-ps')
    ),
    'Macau' => array(
      'mail'     => 'Bonnie.Kuo@partner.bmw.com', 
      'projects' => array('bmw-com-mo')
    ),
    'MX LATAM CARIB' => array(
      'mail'     => 'karen.sanchez@bmw.com', 
      'projects' => array('bmw-bmw-com-mx-1', 'bmw-bmw-antilles-fr', 'bmw-bb', 'bmw-bmw.co.cr', 'bmw-bs', 'bmw-bmw-cl', 'bmw-bmw-com-ar', 'bmw-bmw-com-bo', 'bmw-com-co', 'bmw-bmw-com-do', 'bmw-com-ec', 'bmw-bmw-com-gt', 'bmw-bmw-com-ky', 'bmw-bmw-com-ni', 'bmw-bmw-com-pa', 'bmw-bmw-com-pe-2', 'bmw-bmw-com-py', 'bmw-bmw-com-sv', 'bmw-bmw-com-uy', 'bmw-bmw-cw', 'bmw-bmw-hn', 'bmw-bmw-ht', 'bmw-bmw-lc', 'bmw-bmw-tt', 'bmw-bmwjamaica-com')
    ),
    'Mexico' => array(
      'mail'     => 'gabriela.mai@partner.bmw.com',
      'projects' => array('bmw-bmw-com-mx-1')
    ),
    'Malaysia' => array(
      'mail'     => 'Caryn.Chan@bmw.com.my, Juleen.Tan@bmw.com.my',
      'projects' => array('bmw-bmw-com-my')
    ),
    'Netherlands' => array(
      'mail'     => 'jorne.goosen@bmw.nl, Toine-van-der.Drift@bmw.nl', 
      'projects' => array('bmw-bmw-nl-1')
    ),
    'New Zealand' => array(
      'mail'     => 'stephanie.xavier@bmw.co.nz, Gabrielle.Byfield@bmw.co.nz', 
      'projects' => array('bmw-co-nz')
    ),
    'Portugal' => array(
      'mail'     => 'Filipe.Goncalves@partner.bmw.pt, Catarina.Barata@bmw.pt',
      'projects' => array('bmw-bmw-pt')
    ),
    'Russia' => array(
      'mail'     => 'Philipp.Osadchiy@partner.bmwgroup.com', 
      'projects' => array('bmw-bmw-ru-1')
    ),
    'SE/NO/FI/DK' => array(
      'mail'     => 'christian.porseby@bmw.se, lars-petter.fossheim@bmw.no, sara.asfar@bmw.se, andreas.ekeling@partner.bmw.se, adrian.sjoelund@partner.bmw.se', 
      'projects' => array('bmw-bmw-se', 'bmw-bmw-no', 'bmw-bmw-fi', 'bmw-bmw-dk')
    ),
    'Thailand' => array(
      'mail'     => 'Ladapa.Yodkhuntod@bmw.co.th', 
      'projects' => array('bmw-co-th')
    ),
    'Taiwan' => array(
      'mail'     => 'daphne.lin@bmw.com.tw, Bonnie.Kuo@partner.bmw.com', 
      'projects' => array('bmw-com-tw')
    ),
    'South Africa' => array(
      'mail'     => 'Claycia.Johnson@bmw.co.za, Isabella.Lubbe@partner.bmw.co.za', 
      'projects' => array('bmw-www.bmw.co.za')
    ),
    'Middle East' => array(
      'mail'     => 'Lisa.Steinhauer@bmw.com', 
      'projects' => array('bmw-bmw-az-1', 'bmw-ba', 'bmw-by', 'bmw-bmw-com-al', 'bmw-bmw-com-ge', 'bmw-com-mk', 'bmw-bmw-dz', 'bmw-bmw-eg-com', 'bmw-bmw-is', 'bmw-bmw-kz', 'bmw-bmw-ly', 'bmw-bmw-ma', 'bmw-bmw-md', 'bmw-bmw-mu', 'bmw-bmw-re', 'bmw-bmw-rs', 'bmw-bmw-ua', 'bmw-bmw-voli-me', 'bmw-bmw-co-il', 'bmw-bmw-am')
    ),    
    'CS Region (BG, SK, RO, PL, GR, CZ)' => array(
      'mail'     => 'Alexander.FF.Fischer@bmwgroup.com', 
      'projects' => array('bmw-bg', 'bmw-bmw-sk', 'bmw-bmw-ro', 'bmw-bmw-pl', 'bmw-bmw-gr', 'bmw-bmw-cz')
    ),
    'Bulgaria' => array(
      'mail'     => 'Boyana.Raycheva@bmwgroup.com', 
      'projects' => array('bmw-bg')
    ),
    'Slovakia' => array(
      'mail'     => 'ivana.jakubickova@bmwgroup.com, Lenka.Dermekova@bmwgroup.com', 
      'projects' => array('bmw-bmw-sk')
    ),
    'Slovenia' => array(
      'mail'     => 'Anja.Bizjak@bmwgroup.com', 
      'projects' => array('bmw-bmw-si')
    ),    
    'Romania' => array(
      'mail'     => 'maria.frincu@ideomedia.ro, georgiana.cotolan@ideomedia.ro', 
      'projects' => array('bmw-bmw-ro')
    ),
    'Poland' => array(
      'mail'     => 'Mateusz.Grupa@bmw.pl', 
      'projects' => array('bmw-bmw-pl')
    ),
    'Greece' => array(
      'mail'     => 'Aris.MA.Mexilis@partner.bmw.gr', 
      'projects' => array('bmw-bmw-gr')
    ),
    'Czech Republic' => array(
      'mail'     => 'Kristyna.Feldova@bmwgroup.com', 
      'projects' => array('bmw-bmw-cz')
    ),
    'UK' => array(
      'mail'     => 'Ross.Allum@bmw.co.uk', 
      'projects' => array('bmw-bmw-co-uk')
    ),
    'USA' => array(
      'mail'     => 'Ivone.Aviles@bmwmcext.com, jaime.l.pritchard@bmwna.com', 
      'projects' => array('bmwusa-com')
    ),
    'Turkey' => array(
      'mail'     => 'aysenur.kurt@borusanotomotiv.com',
      'projects' => array('bmw-com-tr')
    ),
    'Cyprus' => array(
      'mail'     => 'grousos@pilakoutasgroup.com.cy', 
      'projects' => array('bmw-com-cy')
    ),
    'Malta' => array(
      'mail'     => 'jformosa@mml.mizzi.com.mt', 
      'projects' => array('bmw-com-mt')
    ),
    'CB-53-sebastian-eschenlohr' => array(
      'mail'     => 'Sebastian.Eschenlohr@bmw.de', 
      'projects' => array('bmw-bmw-co-uk', 'bmw-bmw-ie', 'bmw-bmw-ca', 'bmw-bmw-lao-la')
    ),
    'CB-53-tuukka-salomaa' => array(
      'mail'     => 'Tuukka.Salomaa@bmw.fi', 
      'projects' => array('bmw-www.bmw.fr', 'bmw-www.bmw.be')
    ),
    'CB-53-Kevin-Hofmann' => array(
      'mail'     => 'kevin.hofmann@bmw.de', 
      'projects' => array('bmw-bmw-nl-1', 'bmw-www.bmw.it', 'bmw-bmw-es-1', 'bmw-bmw-pt', 'bmw-www.bmw.co.za')
    ),
    'CB-53-doris-gebhardt' => array(
      'mail'     => 'doris.gebhardt@bmw.de', 
      'projects' => array('bmw-www.bmw.de', 'bmw-www.bmw.ch', 'bmw-at', 'bmw-bg', 'bmw-bmw-cz','bmw-com-cy', 'bmw-bmwaz-ro', 'bmw-bmw-pl','bmw-bmw-gr','bmw-com-mt','bmw-bmw-si','bmw-bmw-sk','bmw-bmw-hu','bmw-bmw-hr','bmw-bmw-com-al','bmw-bmw-am','bmw-bmw-az-1','bmw-ba','bmw-by','bmw-bmw-dz','bmw-bmw-com-ge', 'bmw-bmw-is', 'bmw-bmw-kz', 'bmw-bmw-ly','bmw-bmw-ma','bmw-bmw-md','bmw-bmw-voli-me','bmw-com-mk','bmw-bmw-mu','bmw-bmw-re','bmw-bmw-rs','bmw-bmw-tunisia-com','bmw-bmw-ua')
    ),
    'CB-53-Jennifer-Bishop' => array(
      'mail'     => 'Jennifer.Bishop@bmw.de', 
      'projects' => array('bmw-www.bmw.co.jp','bmw-co-kr','bmw-bmw-in','bmw-bmw-com-my','bmw-co-th','bmw-bmw-com-au','bmw-co-nz','bmw-bmw-co-id', 'bmw-com-tw','bmw-bmwhk-com','bmw-bmw-ru-1', 'bmw-com-bd', 'bmw-bmw-com-bn','bmw-bmw-com-kh','bmw-bmw-lao-la','bmw-bmw-lk','bmw-bmwmyanmar-com','bmw-bmw-nc','bmw-tahiti-com','bmw-bmw-com-ph','bmw-com-sg','bmw-bmw-vn','bmw-bmw-abudhabi-com','bmw-bmw-dubai-com','bmw-bmw-bahrain-com','bmw-bmw-iraq-com','bmw-bmw-jordan-com', 'bmw-bmw-kuwait-com','bmw-bmw-lebanon-com', 'bmw-bmw-oman-com', 'bmw-bmw-pakistan-com', 'bmw-bmw-qatar-com', 'bmw-bmw-saudiarabia-com', 'bmw-bmw-yemen-com')
    )    


  );



  public $sendToTeamsMini   = array(

    'mini-co-uk' => array(
        'mail'     => 'lottie.bullick@mini.co.uk, james.harding@partner.bmw.co.uk, Ben.Levy@wmglobal.com, Charlie.Hutchinson@wmglobal.com, yvonne.ansari@wmglobal.com, matthias.hotz@diva-e.com',
        'projects' => array('mini-co-uk')
    ),  

    'MINI-www.mini.de' => array(
        'mail'     => 'Elisabeth.Kotzian@bmw.de, Gert-ulrich.Grahl@mini.de, y.kashlanmabrouk@plan-net.com, F.Niedermayr@plan-net.com',
        'projects' => array('MINI-www.mini.de')
    ),

    'MINI-www.miniusa.com' => array(
        'mail'     => 'danny.zhu@miniusa.com, rjohnson@criticalmass.com, natew@criticalmass.com, leeg@criticalmass.com, carolineg@criticalmass.com, wmincy@criticalmass.com',
        'projects' => array('MINI-www.miniusa.com')
    ),  

    'MINI-www.mini.fr' => array(
        'mail'     => 'carole.guilbert@mini.fr, sabine.macdonald@bmw.fr, guillaume.pain@wundermangroupe.com, aude.guyon@wundermangroupe.com, estelle.gouezigoux@wundermangroupe.com, pierre.bridet@partner.bmw.fr',
        'projects' => array('MINI-www.mini.fr')
    ),  

    'MINI-www.mini.es' => array(
        'mail'     => 'Antonio.Castro@mini.es, Belen.VA.Vozmediano@partner.mini.es, Esther.Coronado@mini.es, sgonzalezzuazo@deloittedigital.es, afernandezmagdalena@deloittedigital.es',
        'projects' => array('MINI-www.mini.es')
    ),  

    'MINI-www.mini.jp' => array(
        'mail'     => 'Naoko.Yamamura@mini.com, Irene.Nikkein@mini.com, Maeko.Masuda@mini.com',
        'projects' => array('MINI-www.mini.jp')
    ),

    'MINI-www.mini.be' => array(
        'mail'     => 'joachim.sas@mini.be, stijn.vermeulen@mini.be',
        'projects' => array('MINI-www.mini.be')
    ),
    
    'MINI-www.mini.it' => array(
        'mail'     => 'riccardo.licata@mini.com, anna.cerutti@mini.com, yuri.fanini@intesys.it',
        'projects' => array('MINI-www.mini.it')
    ),

    'mini-mini-co-za' => array(
        'mail'     => 'Taz.Ramphisa@mini.co.za, Brilliant.Baloyi@partner.bmw.co.za, Himal.Sonbadhar@vizeum.com, Zoe.Kuscus@partner.bmw.co.za, Sthanda.Manciya@vizeum.com, eghan.Ferguson@vizeum.com',
        'projects' => array('mini-mini-co-za')
    ),

    'aggregate'         => array(
      'mail'     => 'Hanno.Holzer@mini.com, Christoph.Golembusch@mini.com, mini-seo@diva-e.com',
      'projects' => array('MINI-www.miniusa.com', 'MINI-www.mini.it', 'MINI-www.mini.be', 'MINI-www.mini.jp', 'MINI-www.mini.es', 'MINI-www.mini.fr', 'MINI-www.mini.de', 'mini-co-uk', 'mini-mini-co-za')
    )    

  );

	public $projectsWeekly = array(
		'bmw-www.bmw.de'
	);


  public $projectsAdvanced = array(

    'bmw-www.bmw.de'    => 'bmw-www.bmw.de',
    'bmw-www.bmw.be'    => 'bmw-www.bmw.be',
    'bmw-bmw-lu'        => 'bmw-bmw-lu', 
    'bmw-www.bmw.fr'    => 'bmw-www.bmw.fr',
    'bmw-www.bmw.it'    => 'bmw-www.bmw.it',
    'bmw-www.bmw.ch'    => 'bmw-www.bmw.ch',
    'bmw-www.bmw.co.jp' => 'bmw-www.bmw.co.jp'

  );

  public $projectsMiniWorkfront = array(

    'MINI-www.mini.de' => 'MINI-www.mini.de',
    'MINI-www.mini.es' => 'MINI-www.mini.es',
    'MINI-www.mini.fr' => 'MINI-www.mini.fr',
    'MINI-www.mini.at' => 'MINI-www.mini.at',
    'MINI-www.mini.be' => 'MINI-www.mini.be',
    'MINI-www.mini.it' => 'MINI-www.mini.it',
    'MINI-www.mini.jp' => 'MINI-www.mini.jp'

  );


	public $projects = array(
		
		'bmw-tahiti-com',    
		'bmw-bmw-com-kh',
		'bmw-com-bd',
		'bmw-bmw-abudhabi-com',
		'bmw-bmw-antilles-fr',
		'bmw-bmw-bahrain-com',
		'bmw-bmw-dubai-com',
		'bmw-bmw-eg-com',
		'bmw-bmw-iraq-com',
		'bmw-bmw-jordan-com',
		'bmw-bmw-kuwait-com',
		'bmw-bmw-lao-la',
		'bmw-bmw-lebanon-com',
		'bmw-bmw-oman-com',
		'bmw-bmw-pakistan-com',
		'bmw-bmw-qatar-com',
		'bmw-bmw-saudiarabia-com',
		'bmw-bmw-tunisia-com',
		'bmw-bmw-voli-me',
		'bmw-bmw-yemen-com',
		'bmw-bmw-am',
		'bmw-at',
		'bmw-bmw-az-1',
		'bmw-ba',
		'bmw-bb',
		'bmw-www.bmw.be',
		'bmw-bg',
		'bmw-bs',
		'bmw-by',
		'bmw-bmw-ca',
		'bmw-bmw-cc',
		'bmw-www.bmw.ch',
		'bmw-bmw-cl',
		'bmw-bmw.co.cr',
		'bmw-bmw-co-id',
		'bmw-bmw-co-il',
		'bmw-www.bmw.co.jp',
		'bmw-co-nz',
		'bmw-co-kr',
		'bmw-co-th',
		'bmw-bmw-co-uk',
		'bmw-www.bmw.co.za',
		'bmw-bmw-com-al',
		'bmw-bmw-com-ar',
		'bmw-bmw-com-au',
		'bmw-bmw-com-bn',
		'bmw-bmw-com-bo',
		'bmw-com-br',
		'bmw-com-co',
		'bmw-bmw-com-do',
		'bmw-com-ec',
		'bmw-bmw-com-ge',
		'bmw-bmw-com-gt',
		'bmw-bmw-com-ky',
		'bmw-com-mk',
		'bmw-com-mo',
		'bmw-bmw-com-mx-1',
		'bmw-bmw-com-my',
		'bmw-bmw-com-ni',
		'bmw-bmw-com-pa',
		'bmw-bmw-com-pe-2',
		'bmw-bmw-com-ph',
		'bmw-bmw-com-py',
		'bmw-bmw-com-sv',
		'bmw-com-tr',
		'bmw-bmw-com-uy',
		'bmw-bmw-cw',
		'bmw-bmw-cz',
		'bmw-www.bmw.de',
		'bmw-bmw-dk',
		'bmw-bmw-dz',
		'bmw-bmw-es-1',
		'bmw-bmw-fi',
		'bmw-www.bmw.fr',
		'bmw-bmw-gr',
		'bmw-bmw-hn',
		'bmw-bmw-hr',
		'bmw-bmw-ht',
		'bmw-bmw-hu',
		'bmw-bmw-ie',
		'bmw-bmw-in',
		'bmw-bmw-is',
		'bmw-www.bmw.it',
		'bmw-bmw-kz',
		'bmw-bmw-lc',
		'bmw-bmw-lk',
		'bmw-bmw-lu',
		'bmw-bmw-ly',
		'bmw-bmw-ma',
		'bmw-bmw-md',
		'bmw-bmw-mu',
		'bmw-bmw-nc',
		'bmw-bmw-nl-1',
		'bmw-bmw-no',
		'bmw-bmw-pl',
		'bmw-bmw-ps',
		'bmw-bmw-pt',
		'bmw-bmw-re',
		'bmw-bmw-ro',
		'bmw-bmw-rs',
		'bmw-bmw-ru-1',
		'bmw-bmw-se',
		'bmw-bmw-si',
		'bmw-bmw-sk',
		'bmw-bmw-tt',
		'bmw-bmw-ua',
		'bmw-bmw-vn',
		'bmw-bmwhk-com',
		'bmw-bmwjamaica-com',
		'bmw-bmwmyanmar-com',
		'bmwusa-com',
		'bmw-com-sg',
		'bmw-co-nz',
		'bmw-com-cn',
		'bmw-ee',
		'bmw-lt',
		'bmw-lv',
		'bmw-com-tw',
		'bmw-com-cy',
		'bmw-com-mt',

		'mini-minihk-com',
		'MINI-www.mini.at',
		'MINI-www.mini.be',
		'MINI-www.mini.ca',
		'MINI-www.mini.ch',
		'mini-mini-cl',
		'mini-mini-co-cr',
		'MINI-www.mini.co.kr',
		'mini-mini-co-nz',
		'mini-mini-co-th',
		'mini-mini-co-za',
		'mini-mini-com-ar',
		'mini-mini-com-au',
		'mini-mini-com-br',
		'mini-mini-com-co',
		'mini-mini-com-do',
		'mini-mini-com-ec',
		'mini-mini-com-gt',
		'MINI-www.mini.com.mx',
		'mini-mini-com-pa',
		'mini-mini-com-pe',
		'mini-mini-com-py',
		'mini-mini-com-tw',
		'mini-mini-com-uy',
		'MINI-www.mini.de',
		'MINI-www.mini.es',
		'MINI-www.mini.fr',
		'mini-mini-in',
		'MINI-www.mini.it',
		'MINI-www.mini.jp',
		'MINI-www.mini.nl',
		'mini-mini-pt',
		'mini-mini-ru',
		'MINI-www.mini.se',
		'mini-minicaribbean-com',
		'MINI-www.minichina.com.cn',
		'MINI-www.miniusa.com',
		'mini-co-uk'

  );

  public $aggregatechecks   = array(
      'list-http-4xx',
      'list-http-5xx',
      'list-title-missing',
      'list-title-duplicate',
      'list-md-missing',
      'list-md-duplicate',
      'list-h1-missing',
      'list-h1-duplicate',
      'list-images-large',
      'list-http-301',
      'list-http-302',
      'list-title-length',
      'list-md-length',
      'list-robots-noindex',
      'list-word-count',

      'list-h2-missing',
      'list-links-external-number',
      'list-links-internal-number',
      'list-links-internal-nofollow',
      'list-links-internal-none',

      'list-canonical-notself',
      'list-urls-get',
      'list-redirect-chain',
      'list-urls-slow',
      'list-filesize-large',
      'list-robots-conflict',
      'list-canonical-conflict'
    );

	public $listchecks   = array(
      'list-http-301',
      'list-http-302',
      'list-http-302-target',
      'list-http-3xx',
      'list-http-4xx',
      'list-http-5xx',      
      'list-title-missing',
      'list-title-duplicate',
      'list-md-missing',
      'list-md-duplicate',
      'list-h1-missing',
      'list-h1-duplicate',
      'list-images-large',
      'list-title-length',
      'list-md-length',
      'list-robots-noindex',
      'list-word-count',

      'list-h2-missing',
      'list-links-external-number',
      'list-links-internal-number',
      'list-links-internal-nofollow',
      'list-links-internal-none',

      'list-canonical-notself',
      'list-urls-get',
      'list-redirect-chain',
      'list-urls-slow',
      'list-filesize-large',
      'list-robots-conflict',
      'list-canonical-conflict'            
    );  


  public $desc = array (

  // INTER KEY                              TRANSLATION         FULL CSV DOWNLOAD
    'list-http-4xx'                => array('HTTP Status 4xx', 'http-4xx', 'HQ'),
    'list-http-5xx'                => array('HTTP Status 5xx', 'http-5xx', 'HQ'),
    'list-title-missing'           => array('Missing Title Tags', 'title-missing', 'Market'),
    'list-title-duplicate'         => array('Duplicate Title Tags', 'title-duplicate', 'Market'),
    'list-md-missing'              => array('Missing Meta Descriptions', 'md-missing', 'Market'),
    'list-md-duplicate'            => array('Duplicate Meta Descriptions', 'md-duplicate', 'Market'),
    'list-h1-missing'              => array('Missing H1 Tags', 'h1-missing', 'Market'),
    'list-h1-duplicate'            => array('Duplicate H1 Tags', 'h1-duplicate', 'Market'),
    'list-images-large'            => array('Images > 300 KB', 'images-large', 'Market'),
    'list-http-301'				         => array('HTTP Status 301', 'http-301', 'HQ'),
    'list-http-302'				         => array('HTTP Status 302', 'http-302', 'HQ'),
    'list-http-3xx'				         => array('HTTP Status 3xx', 'http-3xx', 'HQ'),
    'list-title-length'            => array('Title Tag - too long/short', 'title-length', 'Market'),
    'list-md-length'			         => array('Meta Description - too long/short', 'md-length', 'Market'),
    'list-word-count'              => array('Pages with less than 300 words', 'word-count', 'Market'),
    'list-robots-noindex'          => array('Pages that will not be indexed by Google', 'robots-noindex', 'Market'),
    'list-h2-missing'              => array('Missing H2 Tags', 'h2-missing', 'Market'),
    'list-links-external-number'   => array('Pages with more than 100 external links', 'links-external-number', 'Market'),
    'list-links-internal-number'   => array('Pages with more than 100 internal links', 'links-internal-number', 'Market'),
    'list-links-internal-nofollow' => array('Pages with internal nofollow links', 'links-internal-nofollow', 'Market'),
    'list-links-internal-none'     => array('Pages with no incoming internal links', 'links-internal-none', 'Market'),
    'list-canonical-notself'       => array('Pages with no self referencing canonical', 'canonical-notself', 'Market'),
    'list-urls-get'                => array('Pages with URL parameter', 'urls-get', 'Market'),
    'list-redirect-chain'          => array('Links with more than one redirect', 'redirect-chain', 'Market'),
    'list-urls-slow'               => array('Slow pages', 'urls-slow', 'Market'),
    'list-filesize-large'          => array('Large pages', 'filesize-large', 'Market'),
    'list-robots-conflict'         => array('Pages with robots conflict', 'robots-conflict', 'Market'), 
    'list-canonical-conflict'      => array('Pages with canonical conflict', 'canonical-conflict', 'Market'),


		'bmw-tahiti-com' => array('www.bmw-tahiti.com'),
		'bmw-bmw-com-kh' => array('www.bmw.com.kh'),
		'bmw-com-bd' => array('www.bmw.com.bd'),
		'bmw-bmw-abudhabi-com' => array('www.bmw-abudhabi.com'),
		'bmw-bmw-antilles-fr' => array('www.bmw-antilles.fr'),
		'bmw-bmw-bahrain-com' => array('www.bmw-bahrain.com'),
		'bmw-bmw-dubai-com' => array('www.bmw-dubai.com'),
		'bmw-bmw-eg-com' => array('www.bmw-eg.com'),
		'bmw-bmw-iraq-com' => array('www.bmw-iraq.com'),
		'bmw-bmw-jordan-com' => array('www.bmw-jordan.com'),
		'bmw-bmw-kuwait-com' => array('www.bmw-kuwait.com'),
		'bmw-bmw-lao-la' => array('www.bmw-lao.la'),
		'bmw-bmw-lebanon-com' => array('www.bmw-lebanon.com'),
		'bmw-bmw-oman-com' => array('www.bmw-oman.com'),
		'bmw-bmw-pakistan-com' => array('www.bmw-pakistan.com'),
		'bmw-bmw-qatar-com' => array('www.bmw-qatar.com'),
		'bmw-bmw-saudiarabia-com' => array('www.bmw-saudiarabia.com'),
		'bmw-bmw-tunisia-com' => array('www.bmw-tunisia.com'),
		'bmw-bmw-voli-me' => array('www.bmw-voli.me'),
		'bmw-bmw-yemen-com' => array('www.bmw-yemen.com'),
		'bmw-bmw-am' => array('www.bmw.am'),
		'bmw-at' => array('www.bmw.at'),
		'bmw-bmw-az-1' => array('www.bmw.az'),
		'bmw-ba' => array('www.bmw.ba'),
		'bmw-bb' => array('www.bmw.bb'),
		'bmw-www.bmw.be' => array('www.bmw.be'),
		'bmw-bg' => array('www.bmw.bg'),
		'bmw-bs' => array('www.bmw.bs'),
		'bmw-by' => array('www.bmw.by'),
		'bmw-bmw-ca' => array('www.bmw.ca'),
		'bmw-bmw-cc' => array('www.bmw.cc'),
		'bmw-www.bmw.ch' => array('www.bmw.ch'),
		'bmw-bmw-cl' => array('www.bmw.cl'),
		'bmw-bmw.co.cr' => array('www.bmw.co.cr'),
		'bmw-bmw-co-id' => array('www.bmw.co.id'),
		'bmw-bmw-co-il' => array('www.bmw.co.il'),
		'bmw-www.bmw.co.jp' => array('www.bmw.co.jp'),
		'bmw-co-nz' => array('www.bmw.co.nz'),
		'bmw-co-kr' => array('www.bmw.co.kr'),
		'bmw-co-th' => array('www.bmw.co.th'),
		'bmw-bmw-co-uk' => array('www.bmw.co.uk'),
		'bmw-www.bmw.co.za' => array('www.bmw.co.za'),
		'bmw-bmw-com-al' => array('www.bmw.com.al'),
		'bmw-bmw-com-ar' => array('www.bmw.com.ar'),
		'bmw-bmw-com-au' => array('www.bmw.com.au'),
		'bmw-bmw-com-bn' => array('www.bmw.com.bn'),
		'bmw-bmw-com-bo' => array('www.bmw.com.bo'),
		'bmw-com-br' => array('www.bmw.com.br'),
		'bmw-com-co' => array('www.bmw.com.co'),
		'bmw-bmw-com-do' => array('www.bmw.com.do'),
		'bmw-com-ec' => array('www.bmw.com.ec'),
		'bmw-bmw-com-ge' => array('www.bmw.com.ge'),
		'bmw-bmw-com-gt' => array('www.bmw.com.gt'),
		'bmw-bmw-com-ky' => array('www.bmw.com.ky'),
		'bmw-com-mk' => array('www.bmw.com.mk'),
		'bmw-com-mo' => array('www.bmw.com.mo'),
		'bmw-bmw-com-mx-1' => array('www.bmw.com.mx'),
		'bmw-bmw-com-my' => array('www.bmw.com.my'),
		'bmw-bmw-com-ni' => array('www.bmw.com.ni'),
		'bmw-bmw-com-pa' => array('www.bmw.com.pa'),
		'bmw-bmw-com-pe-2' => array('www.bmw.com.pe'),
		'bmw-bmw-com-ph' => array('www.bmw.com.ph'),
		'bmw-bmw-com-py' => array('www.bmw.com.py'),
		'bmw-bmw-com-sv' => array('www.bmw.com.sv'),
		'bmw-com-tr' => array('www.bmw.com.tr'),
		'bmw-bmw-com-uy' => array('www.bmw.com.uy'),
		'bmw-bmw-cw' => array('www.bmw.cw'),
		'bmw-bmw-cz' => array('www.bmw.cz'),
		'bmw-www.bmw.de' => array('www.bmw.de'),
		'bmw-bmw-dk' => array('www.bmw.dk'),
		'bmw-bmw-dz' => array('www.bmw.dz'),
		'bmw-bmw-es-1' => array('www.bmw.es'),
		'bmw-bmw-fi' => array('www.bmw.fi'),
		'bmw-www.bmw.fr' => array('www.bmw.fr'),
		'bmw-bmw-gr' => array('www.bmw.gr'),
		'bmw-bmw-hn' => array('www.bmw.hn'),
		'bmw-bmw-hr' => array('www.bmw.hr'),
		'bmw-bmw-ht' => array('www.bmw.ht'),
		'bmw-bmw-hu' => array('www.bmw.hu'),
		'bmw-bmw-ie' => array('www.bmw.ie'),
		'bmw-bmw-in' => array('www.bmw.in'),
		'bmw-bmw-is' => array('www.bmw.is'),
		'bmw-www.bmw.it' => array('www.bmw.it'),
		'bmw-bmw-kz' => array('www.bmw.kz'),
		'bmw-bmw-lc' => array('www.bmw.lc'),
		'bmw-bmw-lk' => array('www.bmw.lk'),
		'bmw-bmw-lu' => array('www.bmw.lu'),
		'bmw-bmw-ly' => array('www.bmw.ly'),
		'bmw-bmw-ma' => array('www.bmw.ma'),
		'bmw-bmw-md' => array('www.bmw.md'),
		'bmw-bmw-mu' => array('www.bmw.mu'),
		'bmw-bmw-nc' => array('www.bmw.nc'),
		'bmw-bmw-nl-1' => array('www.bmw.nl'),
		'bmw-bmw-no' => array('www.bmw.no'),
		'bmw-bmw-pl' => array('www.bmw.pl'),
		'bmw-bmw-ps' => array('www.bmw.ps'),
		'bmw-bmw-pt' => array('www.bmw.pt'),
		'bmw-bmw-re' => array('www.bmw.re'),
		'bmw-bmw-ro' => array('www.bmw.ro'),
		'bmw-bmw-rs' => array('www.bmw.rs'),
		'bmw-bmw-ru-1' => array('www.bmw.ru'),
		'bmw-bmw-se' => array('www.bmw.se'),
		'bmw-bmw-si' => array('www.bmw.si'),
		'bmw-bmw-sk' => array('www.bmw.sk'),
		'bmw-bmw-tt' => array('www.bmw.tt'),
		'bmw-bmw-ua' => array('www.bmw.ua'),
		'bmw-bmw-vn' => array('www.bmw.vn'),
		'bmw-bmwhk-com' => array('www.bmwhk.com'),
		'bmw-bmwjamaica-com' => array('www.bmwjamaica.com'),
		'bmw-bmwmyanmar-com' => array('www.bmwmyanmar.com'),
   	'bmwusa-com'         => array('www.bmwusa.com'),
   	'bmw-com-sg'         => array('www.bmw.com.sg'),
   	'bmw-com-cn'         => array('www.bmw.com.cn'),
   	'bmw-ee'             => array('www.bmw.ee'),
   	'bmw-lv'             => array('www.bmw.lv'),
   	'bmw-lt'             => array('www.bmw.lt'),
		'bmw-com-tw' 				 => array('www.bmw.com.tw'),
		'bmw-com-cy' 				 => array('www.bmw.com.cy'),
		'bmw-com-mt'				 => array('www.bmw.com.mt'),

		'mini-minihk-com' => array('minihk.com'),
		'MINI-www.mini.at' => array('www.mini.at'),
		'MINI-www.mini.be' => array('www.mini.be'),
		'MINI-www.mini.ca' => array('www.mini.ca'),
		'MINI-www.mini.ca-fr' => array('www.mini.ca/fr'),
		'MINI-www.mini.ch' => array('www.mini.ch'),
		'mini-mini-cl' => array('www.mini.cl'),
		'mini-mini-co-cr' => array('www.mini.co.cr'),
		'MINI-www.mini.co.kr' => array('www.mini.co.kr'),
		'mini-mini-co-nz' => array('www.mini.co.nz'),
		'mini-mini-co-th' => array('www.mini.co.th'),
		'mini-mini-co-za' => array('www.mini.co.za'),
		'mini-mini-com-ar' => array('www.mini.com.ar'),
		'mini-mini-com-au' => array('www.mini.com.au'),
		'mini-mini-com-br' => array('www.mini.com.br'),
		'mini-mini-com-co' => array('www.mini.com.co'),
		'mini-mini-com-do' => array('www.mini.com.do'),
		'mini-mini-com-ec' => array('www.mini.com.ec'),
		'mini-mini-com-gt' => array('www.mini.com.gt'),
		'MINI-www.mini.com.mx' => array('www.mini.com.mx'),
		'mini-mini-com-pa' => array('www.mini.com.pa'),
		'mini-mini-com-pe' => array('www.mini.com.pe'),
		'mini-mini-com-py' => array('www.mini.com.py'),
		'mini-mini-com-tw' => array('www.mini.com.tw'),
		'mini-mini-com-uy' => array('www.mini.com.uy'),
		'MINI-www.mini.de' => array('www.mini.de'),
		'MINI-www.mini.es' => array('www.mini.es'),
		'MINI-www.mini.fr' => array('www.mini.fr'),
		'mini-mini-in' => array('www.mini.in'),
		'MINI-www.mini.it' => array('www.mini.it'),
		'MINI-www.mini.jp' => array('www.mini.jp'),
		'MINI-www.mini.nl' => array('www.mini.nl'),
		'mini-mini-pt' => array('www.mini.pt'),
		'mini-mini-ru' => array('www.mini.ru'),
		'MINI-www.mini.se' => array('www.mini.se'),
		'mini-minicaribbean-com' => array('www.minicaribbean.com'),
		'MINI-www.minichina.com.cn' => array('www.minichina.com.cn'),
		'MINI-www.miniusa.com' => array('www.miniusa.com'),
		'mini-co-uk' => array('www.mini.co.uk')

  );


  public $checkmeta = array(

      'list-http-4xx' => array(
        'headlines' => array('Source URL - on this URL the link to the target URL was found', 'Linktext', 'Target URL - where the link from the source URL results', 'Target HTTP status - final HTTP status of the linked URL'), 
        'todo' => array('the link from the source URL must be removed or the link must be changed to an URL that does not respond with a HTTP 4xx', '', '', ''),
        'checks' => array('from_url', 'anchor', 'to_url', 'to_header_status')
      ),

      'list-http-5xx' => array(
        'headlines' => array('Source URL - on this URL the link to the target URL was found', 'Target URL - where the link from the source URL results', 'Target HTTP status - final HTTP status of the linked URL'), 
        'todo' => array('the link from the source URL must be removed or the link must be changed to an URL that does not respond with a HTTP 5xx', '', ''),
        'checks' => array('from_url', 'to_url', 'to_header_status')
      ),    

      'list-title-missing' => array(
        'headlines' => array('URL'), 
        'todo'      => array('Please update the missing title tags'),
        'checks'    => array('url')
      ),      

      'list-md-missing' => array(
        'headlines' => array('URL'), 
        'todo'      => array('Please update the missing meta descriptions'),
        'checks'    => array('url')
      ),      

      'list-h1-missing' => array(
        'headlines' => array('URL'), 
        'todo'      => array('Please update the missing H1 tags'),
        'checks'    => array('url')
      ),      

      'list-title-duplicate' => array(
        'headlines' => array('URL', 'Title', 'amount'), 
        'todo'      => array('Please remove all of the duplicates except for one', ''),
        'checks'    => array('url', 'meta_title', 'count_duplicate_titles')
      ),

      'list-title-length' => array(
        'headlines' => array('URL', 'Title Tag content', 'Title Tag length (characters)'), 
        'todo'      => array('The optimal Title Tag length is between 50 and 60 characters', '', ''),
        'checks'    => array('url', 'meta_title', 'meta_title_length')
      ),

      'list-md-duplicate' => array(
        'headlines' => array('URL', 'Meta Description', 'amount'), 
        'todo'      => array('Please remove all of the duplicates except for one', ''),
        'checks'    => array('url', 'meta_description', 'count_duplicate_descriptions')
      ),      

      'list-md-length' => array(
        'headlines' => array('URL', 'Meta Description content', 'Meta Description length (characters)'), 
        'todo'      => array('The optimal Meta Description length is between 140 and 155 characters', '', ''),
        'checks'    => array('url', 'meta_description', 'meta_description_length')
      ),

      'list-h1-duplicate' => array(
        'headlines' => array('URL', 'H1 content', 'amount'), 
        'todo'      => array('Please remove all of the duplicates except for one', ''),
        'checks'    => array('url', 'h1', 'count_duplicate_h1')
      ),

      'list-images-large' => array(
        'headlines' => array('URL', 'File Size'), 
        'todo'      => array('If possible reduce the filesize', ''),
        'checks'    => array('url', 'file_size')
      ),    

      'list-word-count' => array(
        'headlines' => array('URL', 'Word Count'), 
        'todo'      => array('A SEO relevant website should not have less than 300 words', ''),
        'checks'    => array('url', 'count_words')
      ),

      'list-robots-noindex' => array(
        'headlines' => array('URL', 'Robots Directive'), 
        'todo'      => array('Check if those pages should not be indexed by Google', ''),
        'checks'    => array('url', 'robots')
      ),

      'list-http-301' => array(
        'headlines' => array('Source URL - on this URL the link to the target URL was found', 'Target URL - where the link from the source URL results', 'Target HTTP status - final HTTP status of the linked URL', 'Redirect Target URL - where the redirect results', ''), 
        'todo' => array('the link from the source URL must be changed to the Redirect Target URL', '', '', ''),
        'checks' => array('from_url', 'to_url', 'to_header_status', 'to_passes_juice_to_url')
      ),

      'list-http-302' => array(
        'headlines' => array('Source URL - on this URL the link to the target URL was found', 'Target URL - where the link from the source URL results', 'Target HTTP status - final HTTP status of the linked URL', 'Redirect Target URL - where the redirect results', ''), 
        'todo' => array('the link from the source URL must be changed to the Redirect Target URL', '', '', ''),
        'checks' => array('from_url', 'to_url', 'to_header_status', 'to_passes_juice_to_url')
      ),

      'list-http-302-target' => array(
        'headlines' => array('', '', ''), 
        'todo' => array('the link from the source URL must be changed to the Redirect Target URL', '', ''),
        'checks' => array('url', 'redirect_type_group', 'redirect_to_url')
      ),

      'list-http-3xx' => array(
        'headlines' => array('Source URL - on this URL the link to the target URL was found', 'Target URL - where the link from the source URL results', 'Target HTTP status - final HTTP status of the linked URL', 'Redirect Target URL - where the redirect results', ''), 
        'todo' => array('the link from the source URL must be changed to the Redirect Target URL', '', '', ''),
        'checks' => array('from_url', 'to_url', 'to_header_status', 'to_passes_juice_to_url')
      ),

      'list-h2-missing' => array(
        'headlines' => array('URL'), 
        'todo'      => array('Please update the missing H2 tags'),
        'checks'    => array('url')
      ),      

      'list-links-external-number' => array(
        'headlines' => array('URL', 'Amount of external links'), 
        'todo'      => array('Please review the outgoing external links'),
        'checks'    => array('url', 'count_links_outgoing_external')
      ),

      'list-links-internal-number' => array(
        'headlines' => array('URL', 'Amount of internal links'), 
        'todo'      => array('Please review the outgoing internal links'),
        'checks'    => array('url', 'count_links_outgoing_internal')
      ),

      'list-links-internal-nofollow' => array(
        'headlines' => array('Source URL - on this URL the link to the target URL was found', 'Target URL - where the link from the source URL results'), 
        'todo'      => array('Please review the nofollow internal links. What was the reason for the nofollow?', ''),
        'checks'    => array('from_url', 'to_url')
      ),

      'list-links-internal-none' => array(
        'headlines' => array('URL', 'Amount of incoming internal links'), 
        'todo'      => array('If a site is SEO relevant it should have incoming internal links', ''),
        'checks'    => array('url', 'count_links_incoming_internal')
      ),

      'list-canonical-notself' => array(
        'headlines' => array('URL', 'Canonical to URL'), 
        'todo'      => array('There is no task here if all canonical tags deliberately point to other URLs', ''),
        'checks'    => array('url', 'canonical_to_url')
      ),

      'list-urls-get' => array(
        'headlines' => array('URL', 'Amount of GET parameter'), 
        'todo'      => array('GET parameter change the URL, a canonical tag pointing to the URL without GET parameter should be in place', ''),
        'checks'    => array('url', 'count_get_parameter')
      ),

      'list-redirect-chain' => array(
        'headlines' => array('URL', 'HTTP Status', 'Redirect to', 'Redirect HTTP Status'), 
        'todo'      => array('Redirect chains should be avoided, please compare this report with the HTTP 301 and 302 report to find the source URLs', ''),
        'checks'    => array('url', 'header_status', 'redirect_to_url', 'redirect_to_header_status')
      ),

      'list-urls-slow' => array(
        'headlines' => array('URL', 'Time to first byte', 'Server load time'), 
        'todo'      => array('A slow site could indicate usability and indexing problems', ''),
        'checks'    => array('url', 'server_first_byte', 'server_load_time')
      ),

      'list-filesize-large' => array(
        'headlines' => array('URL', 'File size of the HTML document'), 
        'todo'      => array('A large HTML document could indicate indexing problems', ''),
        'checks'    => array('url', 'file_size')
      ),

      'list-robots-conflict' => array(
        'headlines' => array('URL', 'Robots directive'), 
        'todo'      => array('Please check the URL for multiple robots directives or errors within the robots directive', ''),
        'checks'    => array('url', 'robots_directive_group')
      ),

      'list-canonical-conflict' => array(
        'headlines' => array('URL', 'Canonical directive'), 
        'todo'      => array('Please check the URL for multiple canonical tags or errors within the canonical tag', ''),
        'checks'    => array('url', 'canonical_group')
      )

    );

}

?>
