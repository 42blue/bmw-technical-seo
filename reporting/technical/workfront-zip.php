<?php

require_once('base.inc.php');

class workfrontReport extends ryteBase {

  private $workfrontissues = array('list-http-4xx', 'list-http-5xx', 'list-http-301', 'list-http-302');

  public function __construct () {

  	$this->week_now  = date("W", strtotime('now'));
  	$this->year_now  = date("Y", strtotime('now')); 

    $zip = new ZipArchive;

    foreach ($this->workfrontissues as $issue) {

      unlink(PATH.STOREWORKFRONT. $issue . '.zip');

      if ($zip->open(PATH.STOREWORKFRONT. $issue . '.zip', ZipArchive::CREATE) === TRUE) {

        foreach ($this->projects as $project) {

          // NO MINI
          if (stripos($project, 'mini') !== FALSE) {
            continue;
          }

          // NO ADVANCED
          if (isset($this->projectsAdvanced[$project])) {
            continue;
          }

          $fp = PATH . STORE . $this->year_now . $this->week_now . '/' ;
          $fn = $issue . '_' . $project . '_2020_03.xlsx';

          $zip->addFile($fp.$fn, $fn);

        }

        $zip->close();

      }

    }


    $zip = new ZipArchive;

    foreach ($this->workfrontissues as $issue) {

      unlink(PATH.STOREWORKFRONT. $issue . '-mini.zip');

      if ($zip->open(PATH.STOREWORKFRONT. $issue . '-mini.zip', ZipArchive::CREATE) === TRUE) {

        foreach ($this->projects as $project) {

          // NO MINI
          if (stripos($project, 'bmw') !== FALSE) {
            continue;
          }

          // ONLY DEFINED
          if (!isset($this->projectsMiniWorkfront[$project])) {
            continue;
          }

          $fp = PATH . STORE . $this->year_now . $this->week_now . '/' ;
          $fn = $issue . '_' . $project . '_2020_03.xlsx';

          $zip->addFile($fp.$fn, $fn);

        }

        $zip->close();

      }

    }


  }

}

new workfrontReport();