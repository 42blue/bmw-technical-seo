<?php

// https://oneproseo.advertising.de/oneproapi/bmw/reporting/technical/sendreport-basic-bmw.php

require_once('base.inc.php');

class ryteReport extends ryteBase {

  public $team     = 'BMW';

  public $sendToTest   = array(
    
    'aggregate'         => array(
    	'mail'     => 'm.hotz@advertising.de',
      'projects' => array('bmw-bmw-nl-1', 'bmw-www.bmw.it', 'bmw-bmw-es-1', 'bmw-bmw-pt', 'bmw-www.bmw.co.za')
    ),    

  );


  public function __construct () {

    $this->week_now      = date("W", strtotime('now'));
    $this->month_now     = date("F", strtotime('now'));
    $this->year_now      = date("Y", strtotime('now')); 
    $this->xlsxdate_now  = '_2020_03';

    $this->week_last     = date("W", strtotime('-4 week'));
    $this->month_last    = date("F", strtotime('-1 month'));
    $this->year_last     = date("Y", strtotime('-1 month'));
    $this->xlsxdate_last = '_2020_02';

    //foreach ($this->sendToTeamsBasic as $key => $recipient) {
    foreach ($this->sendToTest as $key => $recipient) {

      if (count ($recipient['projects']) > 0 ) {

        $this->sendto   = $recipient['mail'];
        $this->projects = $recipient['projects'];

        $data_week_now  = $this->readReports($this->year_now.$this->week_now);
        $data_week_last = $this->readReports($this->year_last.$this->week_last);

        $report = $this->createEmail($data_week_now, $data_week_last);
        
        $this->sendEmail($report);

        echo 'SENT '.$key.' TO ' . $this->sendto;
        echo '<br />';

      }

    }

  }


  private function sendEmail ($report) {

   $data = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; padding:10px;">
            <h1 style="color:#fff;"><span style="font-size:45px; font-style:italic;">OneProSEO</span>.enterprise Reporting</h1>
          </td>
        </tr>
        <tr>
          '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
         <td style="text-align: center; padding: 20px; background-color:#f8f8f8;">
          <span style="font-size: 16px; font-color: #636363;">
          Hi '.$this->team.' Team,<br /><br />please find below the monthly report for <b>' . $this->month_now .' / ' . $this->year_now . '</b> of technical and content SEO topics<br />including the raw data as Excel file download.<br /><br /><small>Here you find the  <a href="https://oneproseo.advertising.de/oneproapi/bmw/reporting/faq/" target="_blank">regularly updated FAQ</a> on how to work with the Excel data.</small>'."\n".'
        </td>
        '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
        </tr>
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
        <tr>
          '."\n".'<td rowspan="2" style="width:10px; background-color:#444444;"></td>'."\n".'
          '."\n".'<td style="vertical-align: top; width: 99%">
                    '.$report.'
                  </td>
          '."\n".'<td rowspan="7" style="width:10px; background-color:#444444;"></td>'."\n".'

        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
      </table>';


      $subject  = 'OneProSeo Reporting | '.$this->team.' | Technical SEO';
      $header   = 'MIME-Version: 1.0' . "\r\n" . 'Content-Type: text/html; charset=utf-8' . "\r\n" . 'From: noreply OneProSeo.com <noreply@oneproseo.com>' . "\r\n" . 'Reply-To: bmw-seo@diva-e.com' . "\r\n" ;
      $message  = '<html><head> <style>table {border-spacing: 0;}</style></head><body>';
      $message .= $data;
      $message .= '</body></html>';

      mail($this->sendto, $subject, $message, $header, '-f noreply@oneproseo.com -r noreply@oneproseo.com');
    
  }



  private function createEmail ($data_week_now, $data_week_last) {

    $out = '';

    foreach ($data_week_now as $market => $report) {

      $out .= '<table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; font-size: 16px; border: 1px solid #fff; width: 100%;"><tr>';
      $out .= '<tr>';
      $out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:40%; padding: 5px; border-bottom: 5px solid #444444;">' . $this->desc[$market][0] . '</td>';
			$out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';      
      $out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:22%; padding: 5px; border-bottom: 5px solid #444444;">'. $this->month_last .' / ' . $this->year_last . '</td>';
			$out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>'."\n";
			$out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:100px; padding: 5px; border-bottom: 5px solid #444444;">+/-</td>';			
			$out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>'."\n";			
      $out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:22%; padding: 5px; border-bottom: 5px solid #444444;">'.$this->month_now .' / ' . $this->year_now . '</td>';
			$out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>'."\n";
			$out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:8%; padding: 5px; border-bottom: 5px solid #444444;">Responsible</td>';
      $out .= '</tr>';

      foreach ($this->seochecks as $category => $checkblock) {

        $out .= '<tr>';    
        $out .= '<td colspan="9" style="padding:7px; background-color:#f5f5f5; font-weight: bold; border-bottom: 5px solid #444444; border-top: 5px solid #444444;">' . $category. '</td>';        
        $out .= '</tr>';    

        foreach ($checkblock as $checkname => $elems) {

          $out .= '<tr>';    

          $errors_week_now  = $data_week_now[$market][$checkname] - 2;
          $errors_week_last = $data_week_last[$market][$checkname] - 2;

          $errors_change = '-';
          $diff = $errors_week_now - $errors_week_last;
          if ($errors_week_last < $errors_week_now) {
            $errors_change = '<span style="color: red;">&#9650; +' . $diff . '</span>';
          } else if ($errors_week_last > $errors_week_now) {
            $errors_change = '<span style="color: green;">&#9660; ' . $diff . '</span>';
          }

          if ($errors_week_last < 0) {
            $errors_week_last = 'N/A';
            $errors_week_now = $data_week_now[$market][$checkname] - 2;
            $errors_change = '-';
          }

          $infoboebbel = '<a style="text-decoration:none; font-size:16px; float:right;" href="https://oneproseo.advertising.de/oneproapi/bmw/reporting/faq/#o_'.$checkname.'" target="_blank">&#9432; <small>FAQ</small></a>'."\n";

          $csv_download_last = '<small style="display:inline-block; float:right;">(<a href="' . WWW . $this->year_last.$this->week_last.'/list-'.$elems[1].'_'.$market.$this->xlsxdate_last.'.xlsx">xlsx</a>)</small>';

          $csv_download = '<small style="display:inline-block; float:right;">(<a href="' . WWW . $this->year_now.$this->week_now.'/list-'.$elems[1].'_'.$market.$this->xlsxdate_now.'.xlsx">xlsx</a>)</small>';

          $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' .$elems[0]. ' '.$infoboebbel.'</td>'."\n";
          $out .= '<td style="width:5px; background-color:#444444;"></td>';
          $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' .$errors_week_last. ' ' . $csv_download_last .'</td>'."\n";
          $out .= '<td style="width:5px; background-color:#444444;"></td>';
          $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">'.$errors_change.' </td>'."\n";
          $out .= '<td style="width:5px; background-color:#444444;"></td>';        
          $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' .$errors_week_now. ' ' . $csv_download . '</td>'."\n";
          $out .= '<td style="width:5px; background-color:#444444;"></td>';
          $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;"> '. $elems[2].' </td>'."\n";

          $out .= '</tr>';

        }

      }

      $out .= '</table>';
      $out .= '<div style="background-color:#444444; height:10px; "></div>';

    }
    
    return $out;

  }



  private function readReports ($week) {

    $result = array();

    foreach ($this->projects as $ryteId) {

      foreach ($this->aggregatechecks as $checkname) {

        $fn = PATH.STORE.$week.'/'.$checkname.'_'.$ryteId.'.csv';

        $file = fopen($fn, 'r');

        $arr = array();

        if ($file !== false) {

          while (($line = fgetcsv($file)) !== FALSE) {
            $arr[] = $line[0];
          }          
          fclose($file);

        } else {

          $arr = false;

        }

        $result[$ryteId][$checkname] = count($arr);

      }

    }

    return $result;

  }


}

new ryteReport();