<?php

// 01
// AEM und Server Redirects werden gecheckt 

class checkRedirects
{

  private $result     = array();
  private $timeoffset = 'PT0H';
  private $market     = 'DE';

  public function __construct ()
  {

    $this->readCsv();

    $akamaiHead = array('ruleName','matchURL','result.redirectURL','result.statusCode');
    
    $this->writeResultAkamai($akamaiHead);
    $reminder = array();

    $i=0;
    foreach ($this->basedata as $arr) {

      $redi_origin     = $arr[0];
      $redi_origin_bmw = $arr[0];
      $redi_target_bmw = $arr[1];

      if (empty($redi_origin)) {
        continue;
      }

      if (substr( $redi_origin, 0, 4 ) !== 'http') {

        if (stripos($redi_origin, '/jcr:content') !== FALSE) {
          $redi_origin = str_replace('/jcr:content', '.html', $redi_origin);        
        }

        if (stripos($redi_origin, '/content/bmw/marketDE/bmw_de/de_DE/') !== FALSE) {
          $redi_origin = str_replace('/content/bmw/marketDE/bmw_de/de_DE/', 'https://www.bmw.de/de/', $redi_origin);        
        }

      }

// https://www.bmw.de/de/index

      $f1 = array($redi_origin_bmw, $redi_target_bmw, $redi_origin);
      $f2 = array();

//echo $redi_origin;

      $dset = $this->scrapeCurl($redi_origin);


			echo $redi_origin;
			echo PHP_EOL;
			var_dump($dset[0]);
			echo PHP_EOL;			
			echo $dset[2];
			echo PHP_EOL;

			if ($dset[1] !== '200') {

				foreach ($dset[0] as $key => $redis) {
					$url    = $redis[0];
					$status = $redis[1];
					array_push($f2, $url);
					array_push($f2, $status);
				}

				$dataerror = array_merge ($f1 , $f2);

				$this->writeResultError($dataerror);

			} else {
	      
        $k = 0;
				foreach ($dset[0] as $key => $redis) {
					$url    = $redis[0];
          // CASE OF DIALOG.BMW.COM
          if (stripos($url, 'http') !== 0) {
            $url = $dset[0][$k-1][0];
          }
          $k++;
					$status = $redis[1];
					array_push($f2, $url);
					array_push($f2, $status);
				}

        if (!isset($reminder[$redi_origin])) {
          
          $data = array_merge ($f1 , $f2);
          $this->writeResult($data);

          $dataakamai = array($this->market.$i++, $redi_origin, $url,'301');
          $this->writeResultAkamai($dataakamai);          

        }

        $reminder[$redi_origin] = $redi_origin;
	
			}

    }

  }

  private function readCsv () {
    $this->basedata = array ();   
    $file = fopen('/var/www/oneproapi/bmw/redirect-check/check.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $this->basedata[] = array($line[0], $line[1]);
    }
    fclose($file);
  }

  private function writeResult ($fields) {
    $fp = fopen('/var/www/oneproapi/bmw/redirect-check/result.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResultError ($fields) {
    $fp = fopen('/var/www/oneproapi/bmw/redirect-check/result-error.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResultAkamai ($fields) {
    $fp = fopen('/var/www/oneproapi/bmw/redirect-check/result-akamai.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }


  private function scrapeCurl ($url)
  {

    $finalUrl = '';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');

    $output        = curl_exec($ch);

    $finalURL      = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    $finalHTTPCODE = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	  $size          = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD) / 1000;
    $headerSize    = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $responseHead  = substr($output, 0, $headerSize);
		$responseBody  = substr($output, $headerSize);

    preg_match_all ('|Location: (.*?)\\r\\n|',$responseHead, $result);
    preg_match_all('|HTTP/\d\.\d\s+(\d+)|',$responseHead, $httpResponse);


    // add called url 
		array_unshift ($result[1], $url);
		$i = 0;

    $httpstat = array();
    $lasthttp = 'error';

    foreach ($httpResponse[1] as $httpStatus) {
    	$httpstat[] = array($result[1][$i], $httpStatus); 
    	$lasthttp = $httpStatus;
    	$i++;
    }

    if (curl_exec($ch) === false) {      
      $httpstat[] = array('ERROR', curl_error($ch));
      $lasthttp = curl_error($ch);
    }

    curl_close($ch);

    $xpath = $this->createDomDocument($responseBody);

    $link = $xpath->query('//link[@rel="canonical"]')->item(0);

		$canonical = '';
    if ($link !== null) {
    	$canonical = $link->getAttribute('href');
    }

    return array($httpstat, $lasthttp, $canonical);

  }

  public function createDomDocument ($html) {
    $dom = new DOMDocument ('1.0');
    @$dom->loadHTML($html);
    $xpath = new DOMXpath($dom);
    return $xpath;
  }

  private function dateYMD ()
  {
    $date = new DateTime();
    $date->add(new DateInterval($this->timeoffset));
    $date = $date->getTimestamp();
    return array(date("Y-m-d", $date), date("H:i:s", $date));
  }

}

new checkRedirects ();

?>
