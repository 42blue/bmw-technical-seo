<?php

require('bootstrap.php');

class BMWCsv2Excel {
 
  public function __construct($inputFilePath){

    $date = date("Y", strtotime('now')) . '_' . date("m", strtotime('+ 5 days'));

    $objReader = PHPExcel_IOFactory::createReader('CSV');

    $inputFileName = $inputFilePath . '.csv';

    $objPHPExcel = $objReader->load($inputFileName);

    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

    try {
      $objWriter->save($inputFilePath.'_'.$date.'.xlsx');
    }  catch (Exception $e) {
      echo 'Caught exception: ', $e->getMessage(), "\n";
    }

  }

}


