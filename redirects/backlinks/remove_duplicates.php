<?php

// AEM Redirects

define('DIR', dirname(__FILE__));

class checkProof
{

  private $market     = 'DE';

  public function __construct ()
  {

    $basedata = $this->readCsv();

    $this->checkProof($basedata);

  }


  private function checkProof ($basedata) {

    foreach ($basedata as $url) {

      $dset = $this->scrapeCurl($url);

      $redi_chain = $dset['redis'];
      $last_http  = $dset['finalhttp'];
      $canonical  = $dset['canonical'];

      echo $url;
      echo PHP_EOL;

			foreach ($redi_chain as $key => $redis) {
				$urlr    = $redis[0];
				$status  = $redis[1];
				$lasturl = $urlr;
				$laststatus = $status;
			}

      $arr = array($url, $lasturl, $dset['finalhttp']);

      $this->writeResult($arr);

		}

  }

  private function readCsv () {
    $basedata = array ();   
    $file = fopen(DIR . '/backlinks.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $basedata[$line[0]] = $line[0];
    }
    fclose($file);
    return $basedata;
  }

  private function writeResult ($fields) {
    $fp = fopen(DIR . '/result.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResult2 ($fields) {
    $fp = fopen(DIR . '/result2.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }




  private function scrapeCurl ($url)
  {

    $finalUrl = '';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');

    $output        = curl_exec($ch);

    $finalURL      = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    $finalHTTPCODE = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $size          = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD) / 1000;
    $headerSize    = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $responseHead  = substr($output, 0, $headerSize);
    $responseBody  = substr($output, $headerSize);


    preg_match_all ('|Location: (.*?)\\r\\n|',$responseHead, $result);
    preg_match_all('|HTTP/\d\.\d\s+(\d+)|',$responseHead, $httpResponse);


    // add called url 
    array_unshift ($result[1], $url);
    $i = 0;

    $httpstat = array();
    $lasthttp = 'error';

    foreach ($httpResponse[1] as $httpStatus) {
      $httpstat[] = array($result[1][$i], $httpStatus); 
      $lasthttp = $httpStatus;
      $i++;
    }

    if (curl_exec($ch) === false) {      
      $httpstat[] = array('ERROR', curl_error($ch));
      $lasthttp = curl_error($ch);
    }

    curl_close($ch);

    $xpath = $this->createDomDocument($responseBody);

    $link = $xpath->query('//link[@rel="canonical"]')->item(0);

    $canonical = '';
    if ($link !== null) {
      $canonical = $link->getAttribute('href');
    }

    return array('redis' => $httpstat, 'finalhttp' => $lasthttp, 'canonical' => $canonical);


  }


  public function createDomDocument ($html) {
    $dom = new DOMDocument ('1.0');
    @$dom->loadHTML($html);
    $xpath = new DOMXpath($dom);
    return $xpath;
  }


}

new checkProof ();

?>
