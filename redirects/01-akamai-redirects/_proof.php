<?php

// AEM Redirects

define('DIR', dirname(__FILE__));

class checkProof
{

  public function __construct ()
  {

    $basedata = $this->readCsv();
    //$basedata = $this->readCsvError();

 		$this->checkProof($basedata);

//    $this->checkStatuscode($basedata);

  }

  private function checkStatuscode ($basedata) {

    foreach ($basedata as $set) {

      $redi_target = $set[2];

      $ch = curl_init($redi_target);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_NOBODY, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch, CURLOPT_TIMEOUT,10);
      $output   = curl_exec($ch);
      $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);

echo $httpcode;
echo PHP_EOL;

      if ($httpcode != '200') {
        echo $httpcode;
        echo PHP_EOL;
        echo $redi_target;
        echo PHP_EOL;
        echo PHP_EOL;
      }

    }

  }


  private function checkProof ($basedata) {

    $duplicates = array();

    foreach ($basedata as $set) {

      $redi_origin = $set[1];
      $redi_target = $set[2];     

      if (isset($duplicates[$redi_origin])) {

        echo $redi_origin;
        echo PHP_EOL;

      } else {

        $duplicates[$redi_origin] = $redi_origin;

      }

      if ($redi_origin == $redi_target) {
        echo $redi_target;
        echo PHP_EOL;
      }

      if (stripos($redi_target, $redi_origin) != FALSE) {
        echo $redi_target;
        echo PHP_EOL;
      }

		}

    echo PHP_EOL;
    echo 'ALL URLS: ' . count($basedata);
    echo PHP_EOL;
    echo 'ALL DUPLICATES: ' . count($duplicates);
    echo PHP_EOL;

  }




  private function readCsv () {
    $basedata = array ();   
    $file = fopen(DIR . '/result-akamai.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $basedata[] = array($line[0], $line[1], $line[2]);
    }
    fclose($file);
    return $basedata;
  }

  private function readCsvError () {
    $basedata = array ();   
    $file = fopen(DIR . '/result-error.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $basedata[] = array($line[0], $line[1], $line[4]);
    }
    fclose($file);
    return $basedata;
  }


}

new checkProof ();

?>
