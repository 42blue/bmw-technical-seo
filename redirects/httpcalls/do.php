<?php

// AEM Redirects

define('DIR', dirname(__FILE__));

class checkAEMRedirects
{

  private $specialChar = array('á','Á','é','É','í','Í','ó','Ó','ú','Ú','ñ','Ñ','ü','Ü');

  private $specialCharReplace = array('%C3%A1','%C3%A1','%C3%A9', '%C3%A9', '%C3%AD', '%C3%AD', '%C3%B3','%C3%B3', '%C3%BA', '%C3%BA', '%C3%B1', '%C3%B1', '%C3%BC', '%C3%BC');

  public function __construct ()
  {

    $basedata = $this->readCsv();
    $this->scrapeURLs($basedata);

  }


  private function scrapeURLs ($new) {

    $this->writeResult(array('Akamai Rule ID','Redi Source', 'Redi Target', 'Amount of redirects', 'Final URL', 'Final URL Canonical', 'Canonical Match', 'Canonical and Redi Origin Match'));
    $this->writeResultError(array('Akamai Rule ID','Server Source', 'Server Target', 'Amount of redirects', 'Final URL', 'Final URL HTTP Status'));
    $this->writeResultAkamai(array('ruleName','matchURL','result.redirectURL','result.statusCode'));    

    $i = 0;

    $seenredirect = array();

    foreach ($new as $set) {

      $redi_rule   = $set[0];
			$redi_origin = $set[1];
			$redi_target = $set[2];			

      if (empty($redi_origin)) {
        continue;
      }

      $dset = $this->scrapeCurl($redi_target);

var_dump($dset);
exit;

      $redi_chain = $dset['redis'];
      $last_http  = $dset['finalhttp'];
      $canonical  = $dset['canonical'];

			if ($last_http !== '200') {

        $dataerror = array();

        echo $redi_target;
        echo PHP_EOL;

				foreach ($redi_chain as $key => $redis) {
					$url        = $redis[0];
					$status     = $redis[1];
					$lasturl    = $url;
					$laststatus = $status;
				}

				$redi_hops = count($redi_chain) - 1;
				array_push($dataerror, $redi_hops);
				array_push($dataerror, utf8_encode ($lasturl));
				array_push($dataerror, $laststatus);

				$this->writeResultError($dataerror);

			} else {

			}

		}

  }

  private function readCsv () {
    $basedata = array ();   
    $file = fopen(DIR . '/check.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $basedata[] = array($line[0], $line[1], $line[2]);
    }
    fclose($file);
    return $basedata;
  }

  private function writeResult ($fields) {
    $fp = fopen(DIR . '/result.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResultError ($fields) {
    $fp = fopen(DIR . '/result-error.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

  private function writeResultAkamai ($fields) {
    $fp = fopen(DIR . '/result-akamai.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }


  private function scrapeCurl ($url)
  {

    $finalUrl = '';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');

    $output        = curl_exec($ch);

    $finalURL      = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    $finalHTTPCODE = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	  $size          = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD) / 1000;
    $headerSize    = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $responseHead  = substr($output, 0, $headerSize);
		$responseBody  = substr($output, $headerSize);


    preg_match_all ('|Location: (.*?)\\r\\n|',$responseHead, $result);
    preg_match_all('|HTTP/\d\.\d\s+(\d+)|',$responseHead, $httpResponse);


    // add called url 
		array_unshift ($result[1], $url);
		$i = 0;

    $httpstat = array();
    $lasthttp = 'error';

    foreach ($httpResponse[1] as $httpStatus) {
    	$httpstat[] = array($result[1][$i], $httpStatus); 
    	$lasthttp = $httpStatus;
    	$i++;
    }

    if (curl_exec($ch) === false) {      
      $httpstat[] = array('ERROR', curl_error($ch));
      $lasthttp = curl_error($ch);
    }

    curl_close($ch);

    $responseBody = mb_convert_encoding($responseBody, 'utf-8', mb_detect_encoding($responseBody));
    $responseBody = mb_convert_encoding($responseBody, 'html-entities', 'utf-8');

    $xpath = $this->createDomDocument($responseBody);

    $link = $xpath->query('//link[@rel="canonical"]')->item(0);

		$canonical = '';
    if ($link !== null) {
    	$canonical = str_replace($this->specialChar, $this->specialCharReplace, $link->getAttribute('href'));
    }

    return array('redis' => $httpstat, 'finalhttp' => $lasthttp, 'canonical' => $canonical);


  }

  public function createDomDocument ($html) {
    $dom = new DOMDocument ('1.0');
    @$dom->loadHTML($html);
    $xpath = new DOMXpath($dom);
    return $xpath;
  }

}

new checkAEMRedirects ();

?>
