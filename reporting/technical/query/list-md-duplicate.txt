{
    "action": "list",
    "authentication": {
        "api_key": "%s",
        "project": "%s"
    },
    "pagination": {
        "limit": 10000,
        "offset": 0
    },
    "attributes": [
        "url",
        "meta_description",        
        "count_duplicate_descriptions"
    ],
    "sorting": [
        {
            "attribute": "url",
            "direction": "ASC"
        }
    ],
    "filter": {
        "AND": [
            {
                "field": "header_status",
                "operator": "<",
                "value": 300
            },
            {
                "field": "is_local",
                "operator": "==",
                "value": true
            },
            {
                "field": "meta_description_length",
                "operator": ">",
                "value": 0
            },
            {
                "field": "url_type_id",
                "operator": "==",
                "value": 1
            },
            {
                "AND": [
                    {
                        "field": "count_duplicate_descriptions",
                        "operator": ">",
                        "value": 1
                    }
                ]
            },
            {
                "AND": [
                    {
                        "field": "subdomain",
                        "operator": "LIKE",
                        "value": "www"
                    }
                ]
            }
        ]
    }
}