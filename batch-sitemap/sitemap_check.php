<?php

define('DIR', dirname(__FILE__));

class xmlRead  {

  public function __construct () {

		$sm = array();
		//$sm[] = 'https://www.minihk.com/en_HK/pages.sitemap.xml';
		//$sm[] = 'https://www.mini.at/de_AT/pages.sitemap.xml';
		//$sm[] = 'https://www.mini.be/nl_BE/pages.sitemap.xml';
		//$sm[] = 'https://www.mini.ca/sitemap.xml';
		$sm[] = 'https://www.mini.ch/it_CH/pages.sitemap.xml';
		$sm[] = 'https://www.mini.cl/es_CL/pages.sitemap.xml';
		$sm[] = 'https://www.mini.co.cr/es_CR/pages.sitemap.xml';
		$sm[] = 'https://www.mini.co.kr/ko_KR/pages.sitemap.xml';
		$sm[] = 'http://www.mini.co.nz/sitemap.xml';
		$sm[] = 'https://www.mini.co.th/en_TH/pages.sitemap.xml';
		$sm[] = 'https://www.mini.co.za/en_ZA/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.ar/es_AR/pages.sitemap.xml';
		$sm[] = 'http://www.mini.com.au/sitemap.xml';
		$sm[] = 'https://www.mini.com.br/pt_BR/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.co/es_CO/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.do/es_DO/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.ec/es_EC/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.gt/es_GT/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.mx/es_MX/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.pa/es_PA/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.pe/es_PE/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.py/es_PY/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.tw/zh_TW/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.uy/es_UY/pages.sitemap.xml';
		$sm[] = 'https://www.mini.de/de_DE/pages.sitemap.xml';
		$sm[] = 'https://www.mini.es/es_ES/pages.sitemap.xml';
		$sm[] = 'https://www.mini.fr/fr_FR/pages.sitemap.xml';
		$sm[] = 'https://www.mini.in/en_IN/pages.sitemap.xml';
		$sm[] = 'https://www.mini.it/it_IT/pages.sitemap.xml';
		$sm[] = 'https://www.mini.jp/ja_JP/pages.sitemap.xml';
		$sm[] = 'https://www.mini.nl/nl_NL/pages.sitemap.xml';
		$sm[] = 'https://www.mini.pt/pt_PT/pages.sitemap.xml';
		$sm[] = 'https://www.mini.ru/sitemap.xml';
		$sm[] = 'https://www.mini.se/sitemap.xml';
		$sm[] = 'https://www.minicaribbean.com/en_CC/pages.sitemap.xml';
		$sm[] = 'https://www.miniusa.com/sitemap.xml';
		$sm[] = 'https://www.mini.co.uk/en_GB/pages.sitemap.xml';

		foreach ($sm as $key => $url) {

			$name = str_replace('.', '-', parse_url($url, PHP_URL_HOST));

			echo $path = DIR.'/sitemap-'.$name.'.xml';
			echo PHP_EOL;

    	$this->parseSitemap($path, $name);

		}

  }


  private function parseSitemap($path, $name) {

    $contents = file_get_contents($path);

    $data = new SimpleXMLElement($contents);

    $urls = array();

    foreach ($data->url as $node) {      
      $uri = (string) $node->loc;
      $urls[$uri] = $uri;
    }

    echo PHP_EOL;
    echo count($urls);

    foreach ($urls as $url) {

      file_get_contents($url);

      $resp = $http_response_header[0];
      $resp = str_replace('HTTP/1.0', '', $resp);
      $resp = filter_var($resp, FILTER_SANITIZE_NUMBER_INT);

      $datax   = array();
      $datax[] = array($name, $url, $resp);

      echo $url . ' -> ' . $resp;
      echo PHP_EOL;

      $this->writeCsv($datax);

    }

  }


  public function writeCsv ($newrow) {
    $fp = fopen( 'sitemap_check_result.csv', 'a');
    foreach ($newrow as $fields) {
      fputcsv($fp, $fields);
    }
    fclose($fp);
  }


}

new xmlRead;

?>
