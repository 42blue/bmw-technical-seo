<?php

require_once('../technical/base.inc.php');

class sendreportMan extends ryteBase {

  public function __construct () {


  	if (isset($_POST) && !empty($_POST)) {

      $this->send();

  	}

  	$this->show();

  }


  public function send () {

    if (empty($_POST['report'])) {
      echo '<b>No report selected!</b>';
      exit;
    }

    if (empty($_POST['to'])) {
      echo '<b>No recipient given!</b>';
      exit;
    }

    $emails = explode(PHP_EOL, $_POST['to']);

    $sendto = '';
    foreach ($emails as $mail) {

      if (filter_var(trim($mail), FILTER_VALIDATE_EMAIL)) {
          $sendto .= trim($mail) . ', ';
      }

    }

    if (empty($sendto)) {
      echo '<b>No valid recipient given!</b>';
      exit;
    }

    $projects = $_POST['report'];

    require_once('../technical/sendreport-manual.php');

    $sendto = substr($sendto, 0, -2);

    new ryteReportManual($sendto, $projects);


  }


  public function show () {
  	$out = '<!DOCTYPE html><html><head><style>table {width:800px; margin:30px; border-collapse: collapse;} table, th, td {vertical-align:top; border: 1px solid black; padding:10px;}</style></head><body>';

  	$out .= '<h3>Send Report:</h3> <form action="sendreport.php" method="post"><table><tr><td><b>Select one or multiple reports:</b><br /><small>to select multiple hold STRG button</small><br /><br /><select name="report[]" multiple style="width:300px; height:600px;">';

		$out .= '';

  	foreach ($this->projects as $project) {
	
  		$out .= '<option value="'.$project.'">'.$this->desc[$project][0].'</option>';

  	}

  	$out .= '</select></td><td><b>Send to:</b><br /><small>one email adress per line</small><br /><br /><textarea id="to" name="to"rows="15" cols="40"></textarea></td><td><b>Send:</b><br /><br /><br /><button type="submit" id="berechnen">Send Reports </button></td></tr></table></form>';


		echo $out;

  }


}

new sendreportMan();