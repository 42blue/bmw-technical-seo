<?php

// 03
// Zusammenführen aller Ergebisse 

class checkRedirects
{

  private $result     = array();
  private $timeoffset = 'PT0H';
  private $market     = 'DE';

  public function __construct ()
  {

    $a = $this->readCsvCheck();
    $b = $this->readCsvConvert();    


		$data = array_merge($a,$b);
		$i=0;

    $akamaiHead = array('ruleName','matchURL','result.redirectURL','result.statusCode');
    $this->writeResultAkamai($akamaiHead);

    foreach ($data as $arr) {
    	if($i==0) {
    		$i++;
    		continue;
    	}

			$dataakamai = array($this->market.$i++, $arr[0], $arr[1], '301');
      $this->writeResultAkamai($dataakamai);          

    }

  }

  private function readCsvCheck () {
    $basedata = array ();   
    $file = fopen('/var/www/oneproapi/bmw/redirect-check/result-akamai.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $basedata[$line[1]] = array($line[1], $line[2], $line[3]);
    }
    fclose($file);
    return $basedata;
  }

  private function readCsvConvert () {
    $basedata = array ();   
    $file = fopen('/var/www/oneproapi/bmw/redirect-convert/result-akamai.csv', 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
      $basedata[$line[1]] = array($line[1], $line[2], $line[3]);
    }
    fclose($file);
    return $basedata;
  }

  private function writeResultAkamai ($fields) {
    $fp = fopen('/var/www/oneproapi/bmw/redirect-merge/result-akamai.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }

}

new checkRedirects ();

?>
