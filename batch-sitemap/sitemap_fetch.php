<?php

define('DIR', dirname(__FILE__));

class check
{


  public function __construct () {

		$sm = array();
		$sm[] = 'https://www.minihk.com/en_HK/pages.sitemap.xml';
		$sm[] = 'https://www.mini.at/de_AT/pages.sitemap.xml';
		$sm[] = 'https://www.mini.be/nl_BE/pages.sitemap.xml';
		$sm[] = 'https://www.mini.ca/sitemap.xml';
		$sm[] = 'https://www.mini.ch/it_CH/pages.sitemap.xml';
		$sm[] = 'https://www.mini.cl/es_CL/pages.sitemap.xml';
		$sm[] = 'https://www.mini.co.cr/es_CR/pages.sitemap.xml';
		$sm[] = 'https://www.mini.co.kr/ko_KR/pages.sitemap.xml';
		$sm[] = 'http://www.mini.co.nz/sitemap.xml';
		$sm[] = 'https://www.mini.co.th/en_TH/pages.sitemap.xml';
		$sm[] = 'https://www.mini.co.za/en_ZA/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.ar/es_AR/pages.sitemap.xml';
		$sm[] = 'http://www.mini.com.au/sitemap.xml';
		$sm[] = 'https://www.mini.com.br/pt_BR/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.co/es_CO/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.do/es_DO/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.ec/es_EC/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.gt/es_GT/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.mx/es_MX/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.pa/es_PA/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.pe/es_PE/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.py/es_PY/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.tw/zh_TW/pages.sitemap.xml';
		$sm[] = 'https://www.mini.com.uy/es_UY/pages.sitemap.xml';
		$sm[] = 'https://www.mini.de/de_DE/pages.sitemap.xml';
		$sm[] = 'https://www.mini.es/es_ES/pages.sitemap.xml';
		$sm[] = 'https://www.mini.fr/fr_FR/pages.sitemap.xml';
		$sm[] = 'https://www.mini.in/en_IN/pages.sitemap.xml';
		$sm[] = 'https://www.mini.it/it_IT/pages.sitemap.xml';
		$sm[] = 'https://www.mini.jp/ja_JP/pages.sitemap.xml';
		$sm[] = 'https://www.mini.nl/nl_NL/pages.sitemap.xml';
		$sm[] = 'https://www.mini.pt/pt_PT/pages.sitemap.xml';
		$sm[] = 'https://www.mini.ru/sitemap.xml';
		$sm[] = 'https://www.mini.se/sitemap.xml';
		$sm[] = 'https://www.minicaribbean.com/en_CC/pages.sitemap.xml';
		$sm[] = 'https://www.miniusa.com/sitemap.xml';
		$sm[] = 'https://www.mini.co.uk/en_GB/pages.sitemap.xml';


		foreach ($sm as $key => $url) {

			$name = str_replace('.', '-', parse_url($url, PHP_URL_HOST));

			$res = $this->scrapeCurl($url, $name);

			echo $url . ' - ' . $res['finalhttp'];
			echo PHP_EOL;

			$csv = array($url, $res['finalurl'], $res['finalhttp']);

			$this->writeResult($csv);
			
		}


	
	}


  private function writeResult ($fields) {
    $fp = fopen(DIR . '/sitemap_fetch_result.csv', 'a');
    fputcsv($fp, $fields);
    fclose($fp);
  }



  private function scrapeCurl ($url, $name)
  {

  	$output = '';

    $finalUrl = '';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');

    $output        = curl_exec($ch);

    $finalURL      = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    $finalHTTPCODE = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	  $size          = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD) / 1000;
    $headerSize    = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $responseHead  = substr($output, 0, $headerSize);
		$responseBody  = substr($output, $headerSize);


		$name = 'sitemap-' . $name . '.xml';
    file_put_contents($name, $responseBody);

    preg_match_all ('|Location: (.*?)\\r\\n|',$responseHead, $result);
    preg_match_all('|HTTP/\d\.\d\s+(\d+)|',$responseHead, $httpResponse);

    // add called url 
		array_unshift ($result[1], $url);
		$i = 0;

    $httpstat = array();
    $lasthttp = 'error';

    foreach ($httpResponse[1] as $httpStatus) {
    	$httpstat[] = array($result[1][$i], $httpStatus); 
    	$lasthttp = $httpStatus;
    	$i++;
    }

    if (curl_exec($ch) === false) {      
      $httpstat[] = array('ERROR', curl_error($ch));
      $lasthttp = curl_error($ch);
    }

    curl_close($ch);

    return array('finalurl' => $finalURL, 'finalhttp' => $lasthttp);

  }


}

new check;

?>