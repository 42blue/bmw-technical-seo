<?php

// https://oneproseo.advertising.de/oneproapi/bmw/reporting/technical/sendreport-mini-new.php

require_once('base.inc.php');

class ryteReport extends ryteBase {

  public $sendToTest   = array(

    'aggregate'         => array(
      'mail'     => 'm.hotz@advertising.de',
      'projects' => array('MINI-www.miniusa.com', 'MINI-www.mini.it', 'MINI-www.mini.be', 'MINI-www.mini.jp', 'MINI-www.mini.es', 'MINI-www.mini.fr', 'MINI-www.mini.de', 'mini-co-uk')
    ),    

  );


  public function __construct () {

    $this->week_now      = date("W", strtotime('now'));
    $this->month_now     = date("F", strtotime('now'));
    $this->year_now      = date("Y", strtotime('now')); 
    $this->xlsxdate_now  = '_2020_03';

    $this->week_last     = date("W", strtotime('-4 week'));
    $this->month_last    = date("F", strtotime('-1 month'));
    $this->year_last     = date("Y", strtotime('-1 month'));
    $this->xlsxdate_last = '_2020_02';

    //foreach ($this->sendToTeamsMini as $key => $recipient) {
    foreach ($this->sendToTest as $key => $recipient) {

    	$this->sendto   = $recipient['mail'];
			$this->projects = $recipient['projects'];

	    $data_week_now  = $this->readReports($this->year_now.$this->week_now);
	    $data_week_last = $this->readReports($this->year_last.$this->week_last);

      $this->createPerformanceReport();
	    $this->createEmail($data_week_now, $data_week_last);

	    $this->sendEmail();

    	echo 'SENT '.$key.' TO ' . $this->sendto;
    	echo '<br />';

    }

  }


  private function createPerformanceReport () {

    $this->performancereport = array();

    foreach ($this->projects as $market_id) {      

      $rankings_merge = array();
      // multi langs for one markeet 
      foreach ($this->ops2ryte[$market_id] as $lang => $apidata) {

        $file     = OPSJSON . $apidata['apikey'] . '/rankings_'.$apidata['project'].'_'.$apidata['set'].'_monthly.json'; 

        $json     = file_get_contents($file);
        $rankings = json_decode($json, true);

        $rankings_merge = array_merge($rankings_merge, $rankings);

      }

      if ($json == false || count($rankings_merge) <= 0) {

        $this->performancereport[$market_id] = false;

      } else {

        // sort api data by keyword
        $buffer    = array();
        $timestamp = array();
        $searchv   = array();   
        foreach ($rankings_merge as $x => $dataset) {
          $buffer[$dataset['keyword']][$dataset['timestamp']] = array($dataset['rank'], $dataset['url'], $dataset['sv']);
          $timestamp[] = $dataset['timestamp'];
          $searchv[$dataset['keyword']] = $dataset['sv'];
        }

        // remove first 9 months 
        $reprankings = array();
        foreach ($buffer as $kw => $set) {
          $reprankings[$kw] = array_slice($set, -3, 3, true);
        }

        // sort by searchvolume
        arsort($searchv);

        $timestamp = array_values(array_slice($timestamp, -3, 3, true));

        $out = '';

        $out .= '<table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; font-size: 16px; border: 1px solid #fff; width: 100%;">';
        $out .= '<tr>';
        $out .= '<td style="background-color:#f5f5f5; font-weight: bold; padding: 5px; border-bottom: 5px solid #444444;">Keyword</td>';
        $out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';      
        $out .= '<td style="background-color:#f5f5f5; font-weight: bold; padding: 5px; border-bottom: 5px solid #444444;">Searchvolume / month</td>';
        $out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';      
        $out .= '<td style="background-color:#f5f5f5; font-weight: bold; padding: 5px; border-bottom: 5px solid #444444;">Position<br/>('.$timestamp[2].')</td>';
        $out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';
        $out .= '<td style="background-color:#f5f5f5; font-weight: bold; padding: 5px; border-bottom: 5px solid #444444;">Change/Position: 1 month ago</td>';
        $out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';
        $out .= '<td style="background-color:#f5f5f5; font-weight: bold; padding: 5px; border-bottom: 5px solid #444444;">Change: 3 months ago</td>';
        $out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';
        $out .= '<td style="background-color:#f5f5f5; font-weight: bold; padding: 5px; border-bottom: 5px solid #444444;">Ranking URL</td>';      
        $out .= '</tr>';


        foreach ($searchv as $keyword => $sv) {

          $rank_old1 = '';
          $rank_old2 = '';

          $i = 0;

          foreach ($reprankings[$keyword] as $ts => $values) {

            if ($i == 1) {
              $rank_old1 = $values[0];
            }

            if (empty($rank_old2)) {
              $rank_old2 = $values[0];
            }

            $i++;

            $rank = $values[0];
            $url  = str_replace(' (featured)', '', $values[1]);
            $sv   = $values[2];

          }

          if (empty($rank_old1)) {$rank_old1 = $rank;}
          if (empty($rank_old2)) {$rank_old2 = $rank;}            

          $change1 = $rank_old1 - $rank;
          $change2 = $rank_old2 - $rank;

          if ($change1 > 0) {
            $c1 = '<span style="color: green;">&#9650; +' . $change1 . '</span><small style="float:right">('.$rank_old1.')</small>';
          } else if ($change1 < 0) {
            $c1 = '<span style="color: red;">&#9660;' . $change1 . '</span><small style="float:right">('.$rank_old1.')</small>';
          } else {
            $c1 = ' - <small style="float:right">('.$rank_old1.')</small>';
          }

          if ($change2 > 0) {
            $c2 = '<span style="color: green;">&#9650; +' . $change2 . '</span><small style="float:right">('.$rank_old2.')</small>';
          } else if ($change2 < 0) {
            $c2 = '<span style="color: red;">&#9660;' . $change2 . '</span><small style="float:right">('.$rank_old2.')</small>';
          } else {
            $c2 = ' - <small style="float:right">('.$rank_old2.')</small>';
          }

          $out .= '<tr>';    
          $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' . $keyword . '</td>'."\n";
          $out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';
          $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' . $sv . '</td>'."\n";
          $out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';
          $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' . $rank . '</td>'."\n";
          $out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';
          $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' . $c1 . '</td>'."\n";
          $out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';
          $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' . $c2 . '</td>'."\n";
          $out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';
          $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' . $url . '</td>'."\n";
          $out .= '</tr>';

        }

        $out .= '</table>';

        $this->performancereport[$market_id] = $out;

      }

    }

  }


  private function createEmail ($data_week_now, $data_week_last) {

		$out = '';
    foreach ($data_week_now as $market => $report) {

      if ($this->performancereport[$market] != false) {
        $out .= '<div style="background-color:#444444; padding: 10px 10px 15px;"><a href="https://oneproseo.advertising.de/oneproapi/bmw/reporting/faq/" target="_blank" style="display:inline-block; text-decoration:none; float:right; padding:20px; color:#fff;">&#9432; FAQ</a><h2 style="color:#fff;"><a style="color:#fff;" href="https://'.$this->desc[$market][0].'">'.$this->desc[$market][0].'</a> - Performance Reporting</h2></div>';
        $out .= $this->performancereport[$market];
      }

      $out .= '<div style="background-color:#444444; padding: 10px 10px 15px;"><h2 style="color:#fff;"><a style="color:#fff;" href="https://'.$this->desc[$market][0].'">'.$this->desc[$market][0].'</a> - Technical Reporting</h2></div>';
      $out .= '<table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; font-size: 16px; border: 1px solid #fff; width: 100%;"><tr>';
      $out .= '<tr>';
      $out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:40%; padding: 5px; border-bottom: 5px solid #444444;">' . $this->desc[$market][0] . '</td>';
			$out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';      
			$out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:25%; padding: 5px; border-bottom: 5px solid #444444;">'. $this->month_last .' / ' . $this->year_last . '</td>';
			$out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>';
      $out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:100px; padding: 5px; border-bottom: 5px solid #444444;">+/-</td>';     
      $out .= '<td style="width:5px; background-color:#444444;">&nbsp;</td>'."\n";        
			$out .= '<td style="background-color:#f5f5f5; font-weight: bold; width:25%; padding: 5px; border-bottom: 5px solid #444444;">'.$this->month_now .' / ' . $this->year_now . '</td>';
      $out .= '</tr>';

      foreach ($this->seochecks as $category => $checkblock) {

        $out .= '<tr>';    
        $out .= '<td colspan="7" style="padding:7px; background-color:#f5f5f5; font-weight: bold; border-bottom: 5px solid #444444; border-top: 5px solid #444444;">' . $category. '</td>';        
        $out .= '</tr>';    

        foreach ($checkblock as $checkname => $elems) {

          $out .= '<tr>';    

          $errors_week_now  = $data_week_now[$market][$checkname] - 2;
          $errors_week_last = $data_week_last[$market][$checkname] - 2;

          $errors_change = '-';
          $diff = $errors_week_now - $errors_week_last;
          if ($errors_week_last < $errors_week_now) {
            $errors_change = '<span style="color: red;">&#9650; +' . $diff . '</span>';
          } else if ($errors_week_last > $errors_week_now) {
            $errors_change = '<span style="color: green;">&#9660; ' . $diff . '</span>';
          }

          if ($errors_week_last < 0) {
            $errors_week_last = 'N/A';
            $errors_week_now = $data_week_now[$market][$checkname] - 2;
            $errors_change = '-';
          }

          $infoboebbel = '<a style="text-decoration:none; font-size:16px; float:right;" href="https://oneproseo.advertising.de/oneproapi/bmw/reporting/faq/#o_'.$checkname.'" target="_blank">&#9432; <small>FAQ</small></a>'."\n";

          $csv_download_last = '<small style="display:inline-block; float:right;">(<a href="' . WWW . $this->year_last.$this->week_last.'/list-'.$elems[1].'_'.$market.$this->xlsxdate_last.'.xlsx">xlsx</a>)</small>';

          $csv_download = '<small style="display:inline-block; float:right;">(<a href="' . WWW . $this->year_now.$this->week_now.'/list-'.$elems[1].'_'.$market.$this->xlsxdate_now.'.xlsx">xlsx</a>)</small>';

          $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' .$elems[0]. ' '.$infoboebbel.'</td>'."\n";
          $out .= '<td style="width:5px; background-color:#444444;"></td>';
          $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' .$errors_week_last. ' ' . $csv_download_last .'</td>'."\n";
          $out .= '<td style="width:5px; background-color:#444444;"></td>';
          $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">'.$errors_change.' </td>'."\n";
          $out .= '<td style="width:5px; background-color:#444444;"></td>';        
          $out .= '<td style="padding: 5px; border-bottom: 1px solid #444444;">' .$errors_week_now. ' ' . $csv_download . '</td>'."\n";

          $out .= '</tr>';

        }

      }

      $out .= '</table>';

      $out .= '<div style="background-color:#444444; height:10px; "></div>';

    }

    $this->emailcontent = $out;

  }


  private function sendEmail () {

   $data = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; padding:10px;">
            <h1 style="color:#fff;"><span style="font-size:45px; font-style:italic;">OneProSEO</span>.enterprise Reporting</h1>
          </td>
        </tr>
        <tr>
          '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
         <td style="text-align: center; padding: 20px; background-color:#f8f8f8;">
          <span style="font-size: 16px; font-color: #636363;">
          Hi MINI Team,<br /><br />please find below the monthly report for <b>' . $this->month_now .' / ' . $this->year_now . '</b> of technical and content SEO topics<br />including the raw data as Excel file download.<br /><br /><small>Here you find the  <a href="https://oneproseo.advertising.de/oneproapi/bmw/reporting/faq/" target="_blank">regularly updated FAQ</a> on how to work with the Excel data.</small>'."\n".'
        </td>
        '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
        </tr>
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
        <tr>
          '."\n".'<td rowspan="2" style="width:10px; background-color:#444444;"></td>'."\n".'
          '."\n".'<td style="vertical-align: top; width: 99%">
                    '.$this->emailcontent.'
                  </td>
          '."\n".'<td rowspan="7" style="width:10px; background-color:#444444;"></td>'."\n".'

        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
      </table>';


      $subject  = 'OneProSeo Reporting | MINI | Technical SEO';
      $header   = 'MIME-Version: 1.0' . "\r\n" . 'Content-Type: text/html; charset=utf-8' . "\r\n" . 'From: noreply OneProSeo.com <noreply@oneproseo.com>' . "\r\n" . 'Reply-To: mini-seo@diva-e.com' . "\r\n" ;
      $message  = '<html><head> <style>table {border-spacing: 0;}</style></head><body>';
      $message .= $data;
      $message .= '</body></html>';

      //echo $message;

      mail($this->sendto, $subject, $message, $header, '-f noreply@oneproseo.com -r noreply@oneproseo.com');

  }



  private function readReports ($week) {

    $result = array();

    foreach ($this->projects as $ryteId) {

      foreach ($this->aggregatechecks as $checkname) {

        $fn = PATH.STORE.$week.'/'.$checkname.'_'.$ryteId.'.csv';

        $file = fopen($fn, 'r');

        $arr = array();

        if ($file !== false) {

          while (($line = fgetcsv($file)) !== FALSE) {
            $arr[] = $line[0];
          }          
          fclose($file);

        } else {

          $arr = false;

        }

        $result[$ryteId][$checkname] = count($arr);

      }

    }

    return $result;

  }


}

new ryteReport(); 